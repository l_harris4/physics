#include "cCollision.h"
#include "cTriangle.h"
#include "cGameObject.h"
#include "Physics.h"

cCollision::cCollision()
{
}

cCollision::~cCollision()
{
}

void cCollision::calcCollision()
{
	int gameObjectsSize = otherGameObjects.size();

	if (gameObjectsSize > 0)
	{
		//calculate the response based on other spheres
		BounceCalcSphereSpheres(mainObject, otherGameObjects);
	}
	int trianglesSize = triangles.size();
	if (trianglesSize > 0)
	{
		//calculate the response based on the environment
		calcTrianglesSphereCollision(mainObject, triangles);
	}

}

