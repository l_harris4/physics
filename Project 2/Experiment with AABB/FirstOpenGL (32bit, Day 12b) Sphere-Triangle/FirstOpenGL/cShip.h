#ifndef _CSHIP_HG_
#define _CSHIP_HG_
#include "cGameObject.h"
#include "cPhysicsObject.h"
#include <glm\vec4.hpp>
#include <glm\vec3.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
class cSphere;

class cShip : public cGameObject, public cPhysicsObject
{
public:
	//ctor and dtor
	cShip();
	~cShip();

	//data members
	float radius;
	bool collidedThisFrame;
	bool hittingSphere;
	bool hittingGround;
	bool decellerating = false;
	int health;
	glm::vec3 resetPosition;
	//glm::quat qOrientation;
	glm::vec3 EulerAngles;
	float shieldTimer;
	float shieldTimerReset;
	bool hit = false;
	cSphere* hitSphere;
	//glm::vec3 hitPosition;
	//glm::vec3 hitPositionOffset;

	//methods

	//used to update the sphere based on time
	virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime);

	void ChangeColour(glm::vec4 colour);
	void TakeDamage();
	void Respawn();
	void ChangeRadius(float newRadius);
	void ChangeScale(float newScale);
	glm::vec3 GetHitPosition();

	//quaterion stuff
	void Fly_moveForward(float distanceAlongRelativeZAxis_PosIsForward);
	void Fly_moveRightLeft(float distanceAlongRelativeXAxis_PosIsRight);
	void Fly_moveUpDown(float distanceAlongRelativeYAxis_PosIsUp);
	void Fly_turn_RightLeft(float turnDegreesPosIsRight);
	void Fly_pitch_UpDown(float pitchDegreesPosIsNoseUp);
	void Fly_yaw_CWorCCW(float pitchDegreesPosIsClockWise);
	void overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation);
	void adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange);
};
#endif // !_CSHIP_HG_

