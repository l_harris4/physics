#include "cCollisionManagerImp.h"
#include "cCollision.h"
#include "cTriangle.h"

cCollisionManagerImp::cCollisionManagerImp()
{
}

cCollisionManagerImp::~cCollisionManagerImp()
{
}


void cCollisionManagerImp::CalculateCollisions()
{
	int collisionsSize = collisions.size();
	//call calcCollision on all collisions
	for (int collisionIndex = 0; collisionIndex < collisionsSize; collisionIndex++)
	{
		collisions[collisionIndex].calcCollision();
	}
}

void cCollisionManagerImp::DoStuff()
{
	int n = 1;
}

void cCollisionManagerImp::AddCollision(cCollision collisionData)
{
	collisions.push_back(collisionData);
}

void cCollisionManagerImp::ClearCollisions()
{
	collisions.clear();
}