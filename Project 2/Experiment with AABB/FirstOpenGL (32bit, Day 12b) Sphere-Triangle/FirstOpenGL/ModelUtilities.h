#ifndef _ModelUtilities_HG_
#define _ModelUtilities_HG_

#include <fstream>
#include <string>
#include "cMesh.h"

void ReadFileToToken( std::ifstream &file, std::string token );
bool LoadPlyFileIntoMesh( std::string filename, cMesh &theMesh );
bool LoadPlyFileIntoMeshWithNormals( std::string filename, cMesh &theMesh );




#endif
