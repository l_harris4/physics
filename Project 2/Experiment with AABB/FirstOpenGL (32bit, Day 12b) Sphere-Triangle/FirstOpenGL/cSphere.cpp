#include "cSphere.h"
#include "cTriangle.h"
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include "Utilities.h"	
#include "cCollision.h"
#include "Physics.h"
#include <iostream>
#include "cGameObjectMediator.h"
#include "cShip.h"

extern cGameObjectMediator* g_pGameObjectMediator;

//ctor
cSphere::cSphere()
{
	//set the default values
	this->radius = 0.3f;
	this->collidedThisFrame = false;
	this->editable = true;
	this->health = 3;
	this->hittingSphere = false;
	this->hittingGround = false;

}

//dtor
cSphere::~cSphere()
{
}

//update the sphere based on time
void cSphere::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
{
	
	//update its position based on the position of the ship
	glm::vec4 tempPos = glm::toMat4(theShip->modelData.qOrientation) * offSet;

	modelData.position = glm::vec3(tempPos.x + theShip->modelData.position.x
		, tempPos.y + theShip->modelData.position.y,
		tempPos.z + theShip->modelData.position.z);

	//tell the mediator to compute all the interations
	g_pGameObjectMediator->ComputeInteractions(this);

}

//change a sphere colour
void cSphere::ChangeColour(glm::vec4 colour)
{
	this->modelData.diffuseColour = colour;
	this->modelData.origColour = colour;
}

//reduce health of the sphere and change the colour
void cSphere::TakeDamage()
{
	this->health -= 1;

	if (health <= 0)
	{
		Respawn();
		//health = 3;
		ChangeColour(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
	}
	else if (health == 2)
	{
		ChangeColour(glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
	}
	else if (health == 1)
	{
		ChangeColour(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	}
}

//reset the health and put the sphere back in bounds
void cSphere::Respawn()
{
	health = 3;
	glm::vec3 newPos = glm::vec3(getRandInRange(-20.0f, 10.0f),
		getRandInRange(4.0f, 10.0f),
		getRandInRange(0.0f, -40.0f));
	modelData.position = newPos;
	glm::vec3 newVel = glm::vec3(getRandInRange(-1.0f, 1.0f),
		getRandInRange(-1.0f, 1.0f),
		0);
	velocity = newVel;

	ChangeColour(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
}

//change the radius
void cSphere::ChangeRadius(float newRadius)
{
	this->radius = newRadius;
}

//change the scale
void cSphere::ChangeScale(float newScale)
{
	this->modelData.scale = newScale;
}
