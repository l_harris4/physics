#ifndef _CGAMEOBJECTMEDIATOR_HG_
#define _CGAMEOBJECTMEDIATOR_HG_
#include <vector>
#include <glm\vec3.hpp>

//forward declarations of classes
class cSphere;
class cPhysTriangle;
class iGameObject;
class AABB;
class cShip;

class cGameObjectMediator
{
public:
	//the vector of all game objects
	std::vector< iGameObject* >  vecGameObjects;
	//ctor and dtor
	cGameObjectMediator();
	~cGameObjectMediator();

	int mainObjectIndex = 1;
	//used for moving one sphere
	void MoveMainSphere(glm::vec3 force);
	void TurnShip(glm::vec3 turn);
	void ThrustShip(float thrust);
	void RemoveAccelShip();
	void SetCameraAttributes(glm::vec3& position, glm::vec3& targetPosition);
	//used for updating all the game objects
	void UpdateGameObjects(float deltaTime);
	void SetUpSpheres();
	//used for getting all the colliding triangles for a specific object
	bool cGameObjectMediator::GetCollidingTriangles(glm::vec3 objectPos, float objectRadius, iGameObject* piTriangleObject, std::vector<cPhysTriangle>& closestTriangles);
	bool cGameObjectMediator::GetCollidingTrianglesWithAABB(iGameObject* piSphere, std::vector<cPhysTriangle>& closestTriangles);
	//computing all the interactions for a sphere
	void ComputeInteractions(cSphere* sphere);
	//computing all interactions for a ship
	void cGameObjectMediator::ComputeInteractions(cShip * sphere);
	AABB* mainBox;
	AABB* currentBox;
	void LoadAABB(AABB* theBox);
	glm::vec3 GetHitPosition();
};


#endif // !_CGAMEOBJECTMEDIATOR_HG_

