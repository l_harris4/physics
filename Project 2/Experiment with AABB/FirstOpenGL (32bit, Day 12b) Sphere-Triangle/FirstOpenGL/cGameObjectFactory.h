#ifndef _CGAMEOBJECTFACTORY_HG_
#define _CGAMEOBJECTFACTORY_HG_

#include "iGameObject.h"
#include <string>
#include <vector>

class AABB;

class cGameObjectFactory
{
public:
	//creating a game object //factory pattern
	iGameObject* CreateGameObject(std::string shipType);

	// Builder pattern
	void AssembleGameObject(iGameObject* pTheGameObject, std::string objectType, std::vector<std::string> arugments);
	//used to get the instance //factory pattern
	static cGameObjectFactory* GetInstance();
	
private:
	//the private instance
	static cGameObjectFactory* instance;
	//the private constructor
	cGameObjectFactory() {};

};

#endif // !_CGAMEOBJECTFACTORY_HG_

