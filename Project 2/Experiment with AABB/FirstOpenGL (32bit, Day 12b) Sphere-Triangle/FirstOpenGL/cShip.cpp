#include "cShip.h"
#include "cTriangle.h"
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include "Utilities.h"	
#include "cCollision.h"
#include "Physics.h"
#include <iostream>
#include "cGameObjectMediator.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/glm.hpp>
#include <glm/vec4.hpp>
#include "cShip.h"
#include "cSphere.h"
#include "cLightManager.h"


extern cGameObjectMediator* g_pGameObjectMediator;
extern cLightManager*		g_pLightManager;

//ctor
cShip::cShip()
{
	//set the default values
	this->radius = 1.0f;
	this->collidedThisFrame = false;
	this->editable = true;
	this->health = 3;
	this->hittingSphere = false;
	this->hittingGround = false;
	// Set quaternion to some default rotation value
	this->modelData.qOrientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));
	//this->hitPosition = glm::vec3(0, 0, 0);
	this->shieldTimer = 0.0f;
	this->shieldTimerReset = 3.0f;
	

}

//dtor
cShip::~cShip()
{
}

//update the sphere based on time
void cShip::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
{
	//store the reset position
	this->resetPosition = modelData.position;


	// 2. Directed acceleration
	glm::vec4 directedAccel = glm::vec4(directedAcceleration, 1.0f);
	glm::vec4 directedAccelThisFrame = directedAccel * (float)deltaTime;

	this->directedVelocity += directedAccelThisFrame;

	directedAccelThisFrame.w = 1.0f;

	// 3. Directed velocity

	directedVelocity.w = 1.0f;

	glm::vec4 directedVelThisFrame = directedVelocity;

	glm::mat4 matOrientation = glm::toMat4(this->modelData.qOrientation);

	glm::vec4 velAdjust = matOrientation * directedVelThisFrame;

	velocity += glm::vec3(velAdjust.x, velAdjust.y, velAdjust.z);

	// 3. Linear Velocity

	glm::vec3 accAdjustThisFrame = acceleration * (float)deltaTime;

	velocity += accAdjustThisFrame;

	// 4. Linear position

	glm::vec3 velAdjustThisFrame = velocity * (float)deltaTime;

	if (decellerating)
	{
		directedVelocity *= 0.95;
		velocity *= 0.95;
	}

	modelData.position += velAdjustThisFrame;

	glm::vec4 deltaDirVel = glm::toMat4(modelData.qOrientation) * directedVelocity;

	deltaDirVel *= deltaTime;

	// 5. Update directed velocity from accelleration

	glm::vec4 deltaDirAcc = directedAccel * (float)deltaTime;

	directedVelocity += deltaDirAcc;

	holderVelocity = velocity;


	if (shieldTimer > 0)
	{
		shieldTimer -= deltaTime;
	}
	else
	{
		this->hit = false;
	}
	
	//tell the spotlight what direction we are now looking
	g_pLightManager->vecLights[g_pLightManager->vecLights.size() - 1].position = this->modelData.position +( modelData.qOrientation * glm::vec3(0, 0, 1));
	g_pLightManager->vecLights[g_pLightManager->vecLights.size() - 1].direction = modelData.qOrientation * glm::vec3(0, 0, 1);

}

//change a sphere colour
void cShip::ChangeColour(glm::vec4 colour)
{
	this->modelData.diffuseColour = colour;
	this->modelData.origColour = colour;
}

//reduce health of the sphere and change the colour
void cShip::TakeDamage()
{
}

//reset the health and put the sphere back in bounds
void cShip::Respawn()
{
}

//change the radius
void cShip::ChangeRadius(float newRadius)
{
	this->radius = newRadius;
}

//change the scale
void cShip::ChangeScale(float newScale)
{
	this->modelData.scale = newScale;
}

glm::vec3 cShip::GetHitPosition()
{
	if (hitSphere == nullptr || shieldTimer <= 0)
	{
		return glm::vec3(0, 0, 0);
	}
	else
	{
		return hitSphere->modelData.position;
	}
	return glm::vec3();
}


// ************************************************************
// For the "fly camera":
// +ve is along z-axis
void cShip::Fly_moveForward(float distanceAlongRelativeZAxis_PosIsForward)
{
	// Along the z axis
	this->modelData.position.z += distanceAlongRelativeZAxis_PosIsForward;
	return;
}

void cShip::Fly_moveRightLeft(float distanceAlongRelativeXAxis_PosIsRight)
{
	// Along the x axis
	this->modelData.position.x += distanceAlongRelativeXAxis_PosIsRight;
	return;
}

void cShip::Fly_moveUpDown(float distanceAlongRelativeYAxis_PosIsUp)
{
	// Along the y
	this->modelData.position.y += distanceAlongRelativeYAxis_PosIsUp;
	return;
}

// +ve is right
void cShip::Fly_turn_RightLeft(float turnDegreesPosIsRight)
{
	// From camera orientation, Y axis "turns" left and right
	this->adjustQOrientationFormDeltaEuler(
		glm::vec3(0.0f,
			glm::radians(turnDegreesPosIsRight),	// Y axis
			0.0f));
	return;
}
// +ve it up
void cShip::Fly_pitch_UpDown(float pitchDegreesPosIsNoseUp)
{
	// From camera orientation, X axis "pitches" up and down
	this->adjustQOrientationFormDeltaEuler(
		glm::vec3(glm::radians(pitchDegreesPosIsNoseUp),
			0.0f,
			0.0f));
	return;
}

// +ve is Clock-wise rotation (from nose to tail)
void cShip::Fly_yaw_CWorCCW(float pitchDegreesPosIsClockWise)
{
	// From camera orientation, Z axis "rolls" around
	// (assume z axis is "nose to tail"
	this->adjustQOrientationFormDeltaEuler(
		glm::vec3(0.0f,
			0.0f,
			glm::radians(pitchDegreesPosIsClockWise)));
	return;
}
//// ************************************************************
//
//void cCamera::setOrientationFromEuler(glm::vec3 eulerAngles);
//glm::mat4 cCamera::getMat4FromOrientation(void);

void cShip::overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation)
{
	// Calcualte the quaternion represnetaiton of this Euler axis
	// NOTE: We are OVERWRITING this..
	this->modelData.qOrientation = glm::quat(eulerAxisOrientation);


	return;
}

void cShip::adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange)
{
	// How do we combine two matrices?
	// That's also how we combine quaternions...

	// So we want to "add" this change in oriention
	glm::quat qRotationChange = glm::quat(eulerAxisOrientChange);

	// Mulitply it by the current orientation;
	this->modelData.qOrientation = this->modelData.qOrientation * qRotationChange;

	return;
}

