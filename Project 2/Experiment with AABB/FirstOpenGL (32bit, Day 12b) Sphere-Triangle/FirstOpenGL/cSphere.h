#ifndef _CSPHERE_HG_
#define _CSPHERE_HG_
#include "cGameObject.h"
#include "cPhysicsObject.h"
#include <glm\vec4.hpp>
#include <glm\vec3.hpp>

class cShip;

class cSphere : public cGameObject, public cPhysicsObject
{
public:
	//ctor and dtor
	cSphere();
	~cSphere();

	//data members
	float radius;
	bool collidedThisFrame;
	bool hittingSphere;
	bool hittingGround;
	int health;
	glm::vec3 resetPosition;
	cShip* theShip;
	glm::vec4 offSet;

	//methods

	//used to update the sphere based on time
	virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime);

	void ChangeColour(glm::vec4 colour);
	void TakeDamage();
	void Respawn();
	void ChangeRadius(float newRadius);
	void ChangeScale(float newScale);
};
#endif // !_CSPHERE_HG_

