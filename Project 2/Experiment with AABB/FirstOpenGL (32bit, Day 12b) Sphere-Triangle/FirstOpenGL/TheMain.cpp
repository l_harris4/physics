#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>			// C++ cin, cout, etc.
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include "cCollision.h"
#include "cGameObjectFactory.h"
#include "cModelData.h"
#include "cGameObjectMediator.h"
#include "Physics.h"
#include "cLightManager.h"
#include "AABB.h"
#include "Texture\cBasicTextureManager.h"

//temp here
//#include "cShip.h"

// Forward declaration of functions
void DrawObject(iGameObject* pTheGO);
void PhysicsStep(double deltaTime);
bool LoadModelsLightsFromFile();
bool Load3DModelsIntoMeshManager(int shaderID, cVAOMeshManager* pVAOManager, std::string &error);


glm::vec3 g_cameraXYZOrig = glm::vec3(0.0f, 51.0f, 122.0f);	// 5 units "down" z
glm::vec3 g_cameraTarget_XYZ = glm::vec3(0.0f, 0.0f, 0.0f);
bool g_cameraOnBall = true;

cVAOMeshManager* g_pVAOManager = 0;
cShaderManager*		g_pShaderManager;
cLightManager*		g_pLightManager;
cCollisionManager*  g_pCollisionManager;
cGameObjectMediator* g_pGameObjectMediator;
cBasicTextureManager*	g_pTextureManager = 0;
AABB* theBox;

cGameObject* debugBox;
bool g_drawBoxes = true;
//bool g_bDrawDebugLightSpheres = false;
void DrawDebugBox(glm::vec3 location, glm::vec4 colour, float scale);

// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
							// w = shininess of the 
GLint uniLoc_bIsDebugWireFrameObject = -1;

GLint uniLoc_eyePosition = -1;	// Camera position
GLint uniLoc_hitPosition = -1;	// Camera position
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;

GLint uniLoc_bIsWireframe = -1;
GLint uniLoc_bIsShield = -1;


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

bool bIsShiftPressed(int mods);
bool bIsCtrlPressed(int mods);
bool bIsAltPressed(int mods);
bool bIsShiftPressedAlone(int mods);
bool bIsCtrlPressedAlone(int mods);
bool bIsAltPressedAlone(int mods);

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	if (key == GLFW_KEY_SPACE)
	{
	}

	const float CAMERASPEED = 0.25f;
	switch (key)
	{
	case GLFW_KEY_D:		// Left
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->TurnShip(glm::vec3(0, glm::radians(-1.0), 0));
		//::g_pGameObjectMediator->MoveMainSphere(glm::vec3(-1.0, 0, 0));
		break;
	case GLFW_KEY_A:		// Right
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->TurnShip(glm::vec3(0, glm::radians(1.0), 0));
		//::g_pGameObjectMediator->MoveMainSphere(glm::vec3(+1.0, 0, 0));
		break;
	case GLFW_KEY_S:		// Left
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->TurnShip(glm::vec3(glm::radians(-1.0), 0, 0));
			//::g_pGameObjectMediator->MoveMainSphere(glm::vec3(-1.0, 0, 0));
		break;
	case GLFW_KEY_W:		// Right
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->TurnShip(glm::vec3(glm::radians(+1.0), 0, 0));
			//::g_pGameObjectMediator->MoveMainSphere(glm::vec3(+1.0, 0, 0));
		break;
	case GLFW_KEY_E:		// Forward
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->TurnShip(glm::vec3(0, 0, glm::radians(-1.0)));
			//::g_pGameObjectMediator->MoveMainSphere(glm::vec3(0, 0, -1.0));
		break;
	case GLFW_KEY_Q:		// Backwards (along z)
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->TurnShip(glm::vec3(0, 0, glm::radians(+1.0)));
			//::g_pGameObjectMediator->MoveMainSphere(glm::vec3(0, 0, +1.0));
		break;
	case GLFW_KEY_SPACE:
		if (action == GLFW_PRESS) {
			::g_pGameObjectMediator->MoveMainSphere(glm::vec3(0, 6, 0));
		}
		break;
	case GLFW_KEY_UP:
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
			::g_pGameObjectMediator->ThrustShip(0.1);
		else
			::g_pGameObjectMediator->RemoveAccelShip();
		break;
	case GLFW_KEY_DOWN:
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
			::g_pGameObjectMediator->ThrustShip(-0.1);
		else
			::g_pGameObjectMediator->RemoveAccelShip();
		break;

	}// switch ( key )

	return;
}


int main(void)
{

	GLFWwindow* window;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	int height = 480;	/* default */
	int width = 640;	// default
	std::string title = "OpenGL Rocks";

	std::ifstream infoFile("config.txt");
	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}
	else
	{	// File DID open, so read it... 
		std::string a;

		infoFile >> a;	// "Game"	//std::cin >> a;
		infoFile >> a;	// "Config"
		infoFile >> a;	// "width"

		infoFile >> width;	// 1080

		infoFile >> a;	// "height"

		infoFile >> height;	// 768

		infoFile >> a;		// Title_Start

		std::stringstream ssTitle;		// Inside "sstream"
		bool bKeepReading = true;
		do
		{
			infoFile >> a;		// Title_End??
			if (a != "Title_End")
			{
				ssTitle << a << " ";
			}
			else
			{	// it IS the end! 
				bKeepReading = false;
				title = ssTitle.str();
			}
		} while (bKeepReading);


	}//if ( ! infoFile.is_open() )




	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;


	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Error with shader" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		system("pause");
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;

	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	std::string error;
	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	// Get the uniform locations for this shader
	mvp_location = glGetUniformLocation(currentProgID, "MVP");		// program, "MVP");
	uniLoc_materialDiffuse = glGetUniformLocation(currentProgID, "materialDiffuse");
	uniLoc_materialAmbient = glGetUniformLocation(currentProgID, "materialAmbient");
	uniLoc_ambientToDiffuseRatio = glGetUniformLocation(currentProgID, "ambientToDiffuseRatio");
	uniLoc_materialSpecular = glGetUniformLocation(currentProgID, "materialSpecular");

	uniLoc_bIsDebugWireFrameObject = glGetUniformLocation(currentProgID, "bIsDebugWireFrameObject");

	uniLoc_eyePosition = glGetUniformLocation(currentProgID, "eyePosition");

	

	uniLoc_mModel = glGetUniformLocation(currentProgID, "mModel");
	uniLoc_mView = glGetUniformLocation(currentProgID, "mView");
	uniLoc_mProjection = glGetUniformLocation(currentProgID, "mProjection");
	uniLoc_bIsWireframe = glGetUniformLocation(currentProgID, "bIsWireframe");

	uniLoc_hitPosition = glGetUniformLocation(currentProgID, "hitPosition");
	uniLoc_bIsShield = glGetUniformLocation(currentProgID, "bIsShield");

	::g_pCollisionManager = new cCollisionManager();

	::g_pLightManager = new cLightManager();

	::g_pGameObjectMediator = new cGameObjectMediator();

	::g_pTextureManager = new cBasicTextureManager();
	::g_pTextureManager->SetBasePath("assets/textures");
	//::g_pTextureManager->SetBasePath("");


	//Load the lights and models from a file
	LoadModelsLightsFromFile();


	::g_pLightManager->LoadShaderUniformLocations(currentProgID);

	::g_pLightManager->vecLights[0].attenuation.y = 0.1f;
	::g_pLightManager->vecLights[0].attenuation.z = 0.0f;


	::g_pGameObjectMediator->LoadAABB(theBox);
	::g_pGameObjectMediator->SetUpSpheres();
	glEnable(GL_DEPTH);

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();

	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glm::mat4x4 p, mvp;

		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);

		// Clear colour AND depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		::g_pShaderManager->useShaderProgram("mySexyShader");
		GLint shaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

		// Update all the light uniforms...
		// (for the whole scene)
		::g_pLightManager->CopyLightInformationToCurrentShader();

		// Projection and view don't change per scene (maybe)
		p = glm::perspective(0.6f,			// FOV
			ratio,		// Aspect ratio
			0.1f,			// Near (as big as possible)
			1000.0f);	// Far (as small as possible)

		// View or "camera" matrix
		glm::mat4 v = glm::mat4(1.0f);	// identity

		if (::g_cameraOnBall)
		{
			::g_pGameObjectMediator->SetCameraAttributes(g_cameraXYZOrig, g_cameraTarget_XYZ);
			v = glm::lookAt(g_cameraXYZOrig,						// "eye" or "camera" position
				g_cameraTarget_XYZ,		// "At" or "target" 
				glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
			//glm::vec3 curPostion = ::g_pGameObjectMediator->vecGameObjects[3]->GetModelData()->position;
			//v = glm::lookAt(glm::vec3(curPostion.x, curPostion.y + 10, curPostion.z + 30),						// "eye" or "camera" position
			//	curPostion,		// "At" or "target" 
			//	glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
			//glm::vec3 curPostion = ::g_pGameObjectMediator->vecGameObjects[::g_pGameObjectMediator->mainObjectIndex]->GetModelData()->position;
			//v = glm::lookAt(glm::vec3(curPostion.x, curPostion.y + 1, curPostion.z - 10),						// "eye" or "camera" position
			//	curPostion,		// "At" or "target" 
			//	glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
		}
		else
		{
			v = glm::lookAt(g_cameraXYZOrig,						// "eye" or "camera" position
				g_cameraTarget_XYZ,		// "At" or "target" 
				glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
		}

		glUniformMatrix4fv(uniLoc_mView, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(v));
		glUniformMatrix4fv(uniLoc_mProjection, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(p));

		//// Set ALL texture units and binding for ENTIRE SCENE (is faster)
		//{
		//	// 0 
		//	glActiveTexture(GL_TEXTURE0);
		//	glBindTexture(GL_TEXTURE_2D,
		//		::g_pTextureManager->getTextureIDFromName("Utah_Teapot_xyz_n_uv_Enterprise.bmp"));
		//	//::g_pTextureManager->getTextureIDFromName(pTheGO->textureNames[0]));
		//	// 1
		//	glActiveTexture(GL_TEXTURE1);
		//	glBindTexture(GL_TEXTURE_2D,
		//		::g_pTextureManager->getTextureIDFromName("GuysOnSharkUnicorn.bmp"));
		//	//::g_pTextureManager->getTextureIDFromName(pTheGO->textureNames[1]));
		//	// 2..  and so on... 

		//	// Set sampler in the shader
		//	// NOTE: You shouldn't be doing this during the draw call...
		//	GLint curShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
		//	GLint textSampler00_ID = glGetUniformLocation(curShaderID, "myAmazingTexture00");
		//	GLint textSampler01_ID = glGetUniformLocation(curShaderID, "myAmazingTexture01");
		//	// And so on (up to 10, or whatever number of textures)... 

		//	GLint textBlend00_ID = glGetUniformLocation(curShaderID, "textureBlend00");
		//	GLint textBlend01_ID = glGetUniformLocation(curShaderID, "textureBlend01");

		//	// This connects the texture sampler to the texture units... 
		//	glUniform1i(textSampler00_ID, 0);		// Enterprise
		//	glUniform1i(textSampler01_ID, 1);		// GuysOnSharkUnicorn
		//}


		// Enable blend ("alpha") transparency for the scene
		// NOTE: You "should" turn this OFF, then draw all NON-Transparent objects
		//       Then turn ON, sort objects from far to near ACCORDING TO THE CAMERA
		//       and draw them
		glEnable(GL_BLEND);		// Enables "blending"
								//glDisable( GL_BLEND );
								// Source == already on framebuffer
								// Dest == what you're about to draw
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Draw the scene
		unsigned int sizeOfVector = ::g_pGameObjectMediator->vecGameObjects.size();	//*****//
		for (int index = 0; index != sizeOfVector; index++)
		{
			iGameObject* pTheGO = ::g_pGameObjectMediator->vecGameObjects[index];

			//if (pTheGO->GetType() != "sphere")
			DrawObject(pTheGO);

		}//for ( int index = 0...

		//Draw all the boxes

		//unsigned int sizeOfBoxesVector = ::g_pGameObjectMediator->mainBox->subBoxes.size();
		//for (int index = 0; index != sizeOfBoxesVector; index++)
		//{
		//	DrawDebugBox(::g_pGameObjectMediator->mainBox->subBoxes[index].minPoint,
		//		glm::vec4(1, 0, 0, 1), ::g_pGameObjectMediator->mainBox->subBoxes[index].width);
		//}//for ( int index = 0...

		//for (std::map<unsigned int, AABB>::iterator it = ::g_pGameObjectMediator->mainBox->subBoxes.begin(); it != ::g_pGameObjectMediator->mainBox->subBoxes.end(); ++it)
		//{
		//	DrawDebugBox(it->second.minPoint,
		//				glm::vec4(1, 0, 0, 1), it->second.width);
		//}

		DrawDebugBox(::g_pGameObjectMediator->currentBox->minPoint,
							glm::vec4(1, 0, 0, 1), ::g_pGameObjectMediator->currentBox->width);

		for (int i = 0; i < g_pGameObjectMediator->currentBox->containedTriangles.size(); ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				DrawDebugBox(::g_pGameObjectMediator->currentBox->containedTriangles[i]->vertex[j],
					glm::vec4(1, 0, 0, 1), 0.3);
			}
		}
		

	// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		PhysicsStep(deltaTime);

		lastTimeStep = curTime;

		// Done once per scene 
		glfwSwapBuffers(window);
		glfwPollEvents();
	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(window);
	glfwTerminate();

	// 
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;
	delete ::g_pLightManager;
	delete ::g_pCollisionManager;
	delete ::g_pGameObjectMediator;
	delete theBox;
	return 0;
}

// Update the world 1 "step" in time
void PhysicsStep(double deltaTime)
{
	const glm::vec3 GRAVITY = glm::vec3(0.0f, -9.0f, 0.0f);

	::g_pCollisionManager->ClearCollisions();
	::g_pGameObjectMediator->UpdateGameObjects(deltaTime);

	::g_pCollisionManager->CalculateCollisions();

	return;
}

// Draw a single object
void DrawObject(iGameObject* pTheGO)
{
	// Is there a game object? 
	if (pTheGO == 0)
	{	// Nothing to draw
		return;		// Skip all for loop code and go to next
	}

	// Was near the draw call, but we need the mesh name
	std::string meshToDraw = pTheGO->GetModelData()->meshName;

	sVAOInfo VAODrawInfo;
	if (::g_pVAOManager->lookupVAOFromName(meshToDraw, VAODrawInfo) == false)
	{	// Didn't find mesh
		return;
	}

	// There IS something to draw
	glm::mat4x4 mModel = glm::mat4x4(1.0f);

	glm::mat4 trans = glm::mat4x4(1.0f);
	trans = glm::translate(trans,
		pTheGO->GetModelData()->position);
	mModel = mModel * trans;

	//mat4 RotationMatrix = quaternion::toMat4(quaternion);

	glm::mat4 RotationMatrix = glm::toMat4(pTheGO->GetModelData()->qOrientation);
	mModel = mModel * RotationMatrix;

	float finalScale = pTheGO->GetModelData()->scale;

	glm::mat4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale,
		glm::vec3(finalScale,
			finalScale,
			finalScale));
	mModel = mModel * matScale;

	cModelData* tempData = pTheGO->GetModelData();
	tempData->worldMatrix = mModel;

	glUniformMatrix4fv(uniLoc_mModel, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(mModel));

	glm::mat4 mWorldInTranpose = glm::inverse(glm::transpose(mModel));
	//pTheGO->mModel = mWorldInTranpose;

	glm::vec4 colour = pTheGO->GetModelData()->diffuseColour;

	glUniform4f(uniLoc_materialDiffuse,
		colour.r,
		colour.g,
		colour.b,
		colour.a);
	//...and all the other object material colours


	// Added november 27th
	if (pTheGO->GetModelData()->bIsWireFrame)
	{	// No set bool
		glUniform1f(uniLoc_bIsWireframe, GL_TRUE);		// 1.0
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	// Default
		glCullFace(GL_NONE);
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glUniform1f(uniLoc_bIsWireframe, GL_FALSE);		// 0.0
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// Default
		glCullFace(GL_BACK);
		glEnable(GL_CULL_FACE);
	}


	if (pTheGO->GetType() == "shield")
	{
		glm::vec3 tempPos = ::g_pGameObjectMediator->GetHitPosition();
		glUniform3f(uniLoc_hitPosition,
			tempPos.x,
			tempPos.y,
			tempPos.z);
		glUniform1f(uniLoc_bIsShield, GL_TRUE);
		glCullFace(GL_NONE);
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glUniform1f(uniLoc_bIsShield, GL_FALSE);
	}

	// Set up the textures
	//std::string textureName = pTheGO->textureNames[0];
	//GLuint texture00Number
	//	= ::g_pTextureManager->getTextureIDFromName(textureName);
	// Texture binding... (i.e. set the 'active' texture
	//GLuint texture00Unit = 13;							// Texture units go from 0 to 79 (at least)
	//glActiveTexture(texture00Unit + GL_TEXTURE0);		// GL_TEXTURE0 = 33984
	//glBindTexture(GL_TEXTURE_2D, texture00Number);

	// 0 
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,
		::g_pTextureManager->getTextureIDFromName(pTheGO->textureNames[0]));
	//::g_pTextureManager->getTextureIDFromName("Utah_Teapot_xyz_n_uv_Enterprise.bmp"));
	// 1
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D,
		::g_pTextureManager->getTextureIDFromName(pTheGO->textureNames[1]));
	//::g_pTextureManager->getTextureIDFromName("GuysOnSharkUnicorn.bmp"));
	// 2..  and so on... 

	// Set sampler in the shader
	// NOTE: You shouldn't be doing this during the draw call...
	GLint curShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
	GLint textSampler00_ID = glGetUniformLocation(curShaderID, "myAmazingTexture00");
	GLint textSampler01_ID = glGetUniformLocation(curShaderID, "myAmazingTexture01");
	//// And so on (up to 10, or whatever number of textures)... 

	GLint textBlend00_ID = glGetUniformLocation(curShaderID, "textureBlend00");
	GLint textBlend01_ID = glGetUniformLocation(curShaderID, "textureBlend01");

	//// This connects the texture sampler to the texture units... 
	glUniform1i( textSampler00_ID, 0);		// Enterprise
	//glUniform1i( textSampler01_ID, 1  );		// GuysOnSharkUnicorn
	// .. and so on

	// And the blending values
	glUniform1f(textBlend00_ID, pTheGO->textureBlend[0]);
	glUniform1f(textBlend01_ID, pTheGO->textureBlend[1]);


	if (pTheGO->GetModelData()->bIsWireFrame)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	// Default
													//		glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// Default
		glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
		glEnable(GL_CULL_FACE);
	}

	glCullFace(GL_BACK);

	glBindVertexArray(VAODrawInfo.VAO_ID);

	glDrawElements(GL_TRIANGLES,
		VAODrawInfo.numberOfIndices,		// testMesh.numberOfTriangles * 3,	// How many vertex indices
		GL_UNSIGNED_INT,					// 32 bit int 
		0);
	// Unbind that VAO
	glBindVertexArray(0);

	return;
}

// Used by the light drawing thingy
// Will draw a wireframe sphere at this location with this colour
void DrawDebugBox(glm::vec3 location, glm::vec4 colour,
	float scale)
{
	scale = scale / 2;
	if (debugBox == 0)
	{
		debugBox = new cGameObject();
	}
	glm::vec3 oldPosition = debugBox->modelData.position;
	glm::vec4 oldDiffuse = debugBox->modelData.diffuseColour;
	bool bOldIsWireFrame = debugBox->modelData.bIsWireFrame;

	debugBox->modelData.meshName = "simpleBox.ply";
	debugBox->modelData.position = location;
	debugBox->modelData.diffuseColour = colour;
	debugBox->modelData.bIsWireFrame = true;
	debugBox->modelData.scale = scale;

	DrawObject(::debugBox);

	debugBox->modelData.position = oldPosition;
	debugBox->modelData.diffuseColour = oldDiffuse;
	debugBox->modelData.bIsWireFrame = bOldIsWireFrame;

	return;
}



bool bIsShiftPressed(int mods)
{
	if (mods & GLFW_MOD_SHIFT)
	{
		return true;
	}
	return false;
}

bool bIsCtrlPressed(int mods)
{
	if (mods & GLFW_MOD_CONTROL)
	{
		return true;
	}
	return false;
}

bool bIsAltPressed(int mods)
{
	if (mods & GLFW_MOD_ALT)
	{
		return true;
	}
	return false;
}


bool bIsShiftPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_SHIFT) == GLFW_MOD_SHIFT)
	{
		return true;
	}
	return false;
}

bool bIsCtrlPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL)
	{
		return true;
	}
	return false;
}

bool bIsAltPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_ALT) == GLFW_MOD_ALT)
	{
		return true;
	}
	return false;
}