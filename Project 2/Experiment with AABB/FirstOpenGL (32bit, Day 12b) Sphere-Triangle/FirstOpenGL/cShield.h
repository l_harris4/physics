#ifndef _CSHIELD_HG_
#define _CSHIELD_HG_
#include "cGameObject.h"
#include "cPhysicsObject.h"
#include <glm\vec4.hpp>
#include <glm\vec3.hpp>

class cShip;

class cShield : public cGameObject, public cPhysicsObject
{
public:
	//ctor and dtor
	cShield();
	~cShield();

	//data members
	float radius;
	bool collidedThisFrame;
	bool hittingSphere;
	bool hittingGround;
	int health;
	glm::vec3 resetPosition;
	cShip* theShip;
	glm::vec4 offSet;

	//methods

	//used to update the sphere based on time
	virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime);
};
#endif // !_CSHIELD_HG_

