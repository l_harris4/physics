#ifndef _CMISSILE_HG_
#define _CMISSILE_HG_

#include "cGameObject.h"
#include "cPhysicsObject.h"

//new class to demonstrate builder
class cMissile: public cGameObject, public cPhysicsObject
{
public:
	//ctor
	cMissile();
	~cMissile();
	//is the missile fired or not
	bool fired;
	//fire the missile
	void Fire();
	//update the missile based on time
	virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime);
};


#endif // !_CMISSILE_HG_

