#include "AABB.h"


extern cVAOMeshManager* g_pVAOManager;

AABB::AABB()
{
	u0 = glm::vec3(1, 0, 0);
	u1 = glm::vec3(0, 1, 0);
	u2 = glm::vec3(0, 0, 1);
}

unsigned int CalcBoxID(glm::vec3 pos)
{
	const unsigned long long Z_MULT = 1;
	const unsigned long long Y_MULT = 100;
	const unsigned long long X_MULT = 10000;

	unsigned int ID =
		((int)(pos.x) * X_MULT) +		// ?xxxxx,------,------
		((int)(pos.y) * Y_MULT) +		// ------,?yyyyy,------
		((int)(pos.z)); //* Z_MULT;		// ------,------,?zzzzz

	return ID;
}

AABB::AABB(cGameObject* worldObject)
{
	//Find the extents of the object
	width = calcGameObjectExtent(worldObject);
	//use that to create the huge cube
	//then find the largest triangle
	//use the extends of that (times 3 or something) to create all the smaller cubes

	float subWidth = width / divisionNumber;
	//breaks somewhere in these loops
	for (int i = 0; i < divisionNumber; ++i)
	{
		for (int j = 0; j < divisionNumber; ++j)
		{
			for (int k = 0; k < divisionNumber; ++k)
			{
				AABB temp;
				temp.parent = this;
				temp.minPoint = glm::vec3(minPoint.x + (subWidth * i), minPoint.y + (subWidth * j), minPoint.z + (subWidth * k));
				temp.width = subWidth;
				temp.p_VAODrawInfo = p_VAODrawInfo;
				//subBoxes.push_back(temp);
				//map stuff
				unsigned int tempId = CalcBoxID(temp.minPoint);
				subBoxes[tempId] = temp;
				
			}
		}
	}
	////Then go through and load each smaller cube with a vector of pointers to triangle that it contains
	//int size = subBoxes.size();
	//for (int i = 0; i < size; ++i)
	//{
	//	subBoxes[i].findContainedTriangles();
	//}

	//for (int i = 0; i < size; ++i)
	//{
	//	subBoxes[i].FillOutAABB();
	//}
	for (std::map<unsigned int, AABB>::iterator it = subBoxes.begin(); it != subBoxes.end(); ++it)
	{
		it->second.findContainedTriangles();
	}
	for (std::map<unsigned int, AABB>::iterator it = subBoxes.begin(); it != subBoxes.end(); ++it)
	{
		if (it->second.containedTriangles.size() > 0)
		{
			it->second.FillOutAABB();
		}
	}
}


AABB::~AABB()
{
	//delete p_VAODrawInfo;
}

float AABB::calcGameObjectExtent(cGameObject * worldObject)
{
	//p_VAODrawInfo = new sVAOInfo();

	p_VAODrawInfo = g_pVAOManager->lookupVAOFromName(worldObject->modelData.meshName);
	//p_VAODrawInfo = &VAODrawInfo;
	sVAOInfo VAODrawInfo = (*p_VAODrawInfo);

	glm::vec3 minXYZ(0.0f);
	glm::vec3 maxXYZ(0.0f);
	glm::vec3 maxExtentXYZ(0.0f);

	// Assume 1st vertex is both max and min
	minXYZ.x = VAODrawInfo.vecPhysTris[0].vertex[0].x;
	minXYZ.y = VAODrawInfo.vecPhysTris[0].vertex[0].y;
	minXYZ.z = VAODrawInfo.vecPhysTris[0].vertex[0].z;
	maxXYZ.x = VAODrawInfo.vecPhysTris[0].vertex[0].x;
	maxXYZ.y = VAODrawInfo.vecPhysTris[0].vertex[0].y;
	maxXYZ.z = VAODrawInfo.vecPhysTris[0].vertex[0].z;

	int size = VAODrawInfo.vecPhysTris.size();
	for (int triIndex = 0; triIndex != size; triIndex++)
	{
		for (int vertexIndex = 0; vertexIndex < 3; vertexIndex++)
		{
			if (VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].x < minXYZ.x)
			{
				minXYZ.x = VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].x;
			}
			if (VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].x > maxXYZ.x)
			{
				maxXYZ.x = VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].x;
			}
			if (VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].y < minXYZ.y)
			{
				minXYZ.y = VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].y;
			}
			if (VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].y > maxXYZ.y)
			{
				maxXYZ.y = VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].y;
			}
			if (VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].z < minXYZ.z)
			{
				minXYZ.z = VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].z;
			}
			if (VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].z > maxXYZ.z)
			{
				maxXYZ.z = VAODrawInfo.vecPhysTris[triIndex].vertex[vertexIndex].z;
			}
		}
	}//for ( int index...

	minPoint = minXYZ;

	maxExtentXYZ.x = maxXYZ.x - minXYZ.x;
	maxExtentXYZ.y = maxXYZ.y - minXYZ.y;
	maxExtentXYZ.z = maxXYZ.z - minXYZ.z;

	float maxExtent = 0.0f;
	maxExtent = maxExtentXYZ.x;
	if (maxExtent < maxExtentXYZ.y)
	{	// Y is bigger
		maxExtent = maxExtentXYZ.y;
	}
	if (maxExtent < maxExtentXYZ.z)
	{	// Z is bigger
		maxExtent = maxExtentXYZ.z;
	}
	//

	return maxExtent;
}

void AABB::findContainedTriangles()
{
	//p_VAODrawInfo = new sVAOInfo();
	//sVAOInfo VAODrawInfo = (*p_VAODrawInfo);

	int size = (*p_VAODrawInfo).vecPhysTris.size();
	for (int triIndex = 0; triIndex != size; triIndex++)
	{
		for (int vertexIndex = 0; vertexIndex < 3; vertexIndex++)
		{
			glm::vec3 point = (*p_VAODrawInfo).vecPhysTris[triIndex].vertex[vertexIndex];
			if (point.x >= minPoint.x &&
				point.x <= (minPoint.x + width) &&
				point.y >= minPoint.y &&
				point.y <= (minPoint.y + width) &&
				point.z >= minPoint.z &&
				point.z <= (minPoint.z + width))//if the vertex if within the box, add it to the vector of tris
			{
				containedTriangles.push_back(&(*p_VAODrawInfo).vecPhysTris[triIndex]);
				break;
			}
		}
		//if it made it here then it thinks the triangle is not in the AABB
		//so test if the triangle might have been too big to fit in the AABB
		if (LargestSide((*p_VAODrawInfo).vecPhysTris[triIndex].vertex[0],
			(*p_VAODrawInfo).vecPhysTris[triIndex].vertex[1],
			(*p_VAODrawInfo).vecPhysTris[triIndex].vertex[2]) > width / 2.0f)
		{
			//do the ericson test on if the triangle is in the AABB
			if (TestTriangleAABB((*p_VAODrawInfo).vecPhysTris[triIndex].vertex[0],
				(*p_VAODrawInfo).vecPhysTris[triIndex].vertex[1],
				(*p_VAODrawInfo).vecPhysTris[triIndex].vertex[2]))
				containedTriangles.push_back(&(*p_VAODrawInfo).vecPhysTris[triIndex]);
		}
	}//for ( int index...
}

void AABB::findContainedTriangles(std::vector<cPhysTriangle*> const & parentsTriangles)
{

	int size = parentsTriangles.size();
	for (int triIndex = 0; triIndex != size; triIndex++)
	{
		for (int vertexIndex = 0; vertexIndex < 3; vertexIndex++)
		{
			glm::vec3 point = parentsTriangles[triIndex]->vertex[vertexIndex];
			//glm::vec3 point = parentsTriangles[triIndex].vertex[vertexIndex];
			if (point.x >= minPoint.x &&
				point.x <= (minPoint.x + width) &&
				point.y >= minPoint.y &&
				point.y <= (minPoint.y + width) &&
				point.z >= minPoint.z &&
				point.z <= (minPoint.z + width))//if the vertex if within the box, add it to the vector of tris
			{
				containedTriangles.push_back(parentsTriangles[triIndex]);
				break;
			}
		}

		//if it made it here then it thinks the triangle is not in the AABB
		//so test if the triangle might have been too big to fit in the AABB
		if (LargestSide(parentsTriangles[triIndex]->vertex[0],
			parentsTriangles[triIndex]->vertex[1],
			parentsTriangles[triIndex]->vertex[2]) > width / 2.0f)
		{
			//do the ericson test on if the triangle is in the AABB
			if (TestTriangleAABB(parentsTriangles[triIndex]->vertex[0],
				parentsTriangles[triIndex]->vertex[1],
				parentsTriangles[triIndex]->vertex[2]))
				containedTriangles.push_back(parentsTriangles[triIndex]);
		}
	}//for ( int index...
}

float Max(float a, float b)
{
	if (a > b)
		return a;
	else
		return b;
}

float Max(float a, float b, float c)
{
	float highest = a;
	if (b > highest)
		highest = b;
	if (c > highest)
		highest = c;

	return highest;
}


float Min(float a, float b, float c)
{
	float lowest = a;
	if (b < lowest)
		lowest = b;
	if (c < lowest)
		lowest = c;

	return lowest;
}


float Min(float a, float b)
{
	float lowest = a;
	if (b < lowest)
		lowest = b;
	return lowest;
}




void AABB::FillOutAABB()
{
	float subWidth = width / divisionNumber;
	//breaks somewhere in these loops
	for (int i = 0; i < divisionNumber; ++i)
	{
		for (int j = 0; j < divisionNumber; ++j)
		{
			for (int k = 0; k < divisionNumber; ++k)
			{
				AABB temp;
				temp.parent = this;
				temp.minPoint = glm::vec3(minPoint.x + (subWidth * i), minPoint.y + (subWidth * j), minPoint.z + (subWidth * k));
				temp.width = subWidth;
				temp.p_VAODrawInfo = p_VAODrawInfo;
				//subBoxes.push_back(temp);
				temp.findContainedTriangles(containedTriangles);
				if (temp.containedTriangles.size() > 0) {
					//map stuff
					subBoxes[CalcBoxID(temp.minPoint)] = temp;
				}
			}
		}
	}
	//Then go through and load each smaller cube with a vector of pointers to triangle that it contains
	/*for (std::map<unsigned int, AABB>::iterator it = subBoxes.begin(); it != subBoxes.end(); ++it)
	{
		it->second.findContainedTriangles(containedTriangles);
	}*/
	/*int size = subBoxes.size();
	for (int i = 0; i < size; ++i)
	{
		subBoxes[i].findContainedTriangles(containedTriangles);
	}*/
}

//following function taken from christer ericsons real time collision detection book
bool AABB::TestTriangleAABB(glm::vec3 v0, glm::vec3 v1, glm::vec3 v2)
{
	float p0, p1, p2, r;

	//Compute box center and extents(if not already given in that format)
	glm::vec3 c = glm::vec3(minPoint.x + (width / 2),
		minPoint.y + (width / 2),
		minPoint.z + (width / 2));
	//This next part is way simpler because we are dealing with a cube
	float extent = width;

	//Translate triangle as conceptually moving AABB to origin
	v0 = v0 - c;
	v1 = v1 - c;
	v2 = v2 - c;

	//Compute edge vectors for triangle
	glm::vec3 f0 = v1 - v2;
	glm::vec3 f1 = v2 - v1;
	glm::vec3 f2 = v0 - v2;

	glm::vec3 a00 = glm::cross(u0, f0);
	glm::vec3 a01 = glm::cross(u0, f1);
	glm::vec3 a02 = glm::cross(u0, f2);
	glm::vec3 a10 = glm::cross(u1, f0);
	glm::vec3 a11 = glm::cross(u1, f1);
	glm::vec3 a12 = glm::cross(u1, f2);
	glm::vec3 a20 = glm::cross(u2, f0);
	glm::vec3 a21 = glm::cross(u2, f1);
	glm::vec3 a22 = glm::cross(u2, f2);


	// Test axis a00
	p0 = v0.z * v1.y - v0.y * v1.z;
	p2 = v2.z *(v1.y - v0.y) - v2.z*(v1.z - v0.z);
	r = CalcR(extent, a00);
	if (Max(-Max(p0, p2), Min(p0, p2)) > r) return false; //this can usually be simplified depending on which 2 are the same
	//Test the axis a01
	p0 = -v0.y * (v2.z - v1.z) + v0.z * (v2.y - v1.y);
	p0 = glm::dot(v0, a00);
	p1 = glm::dot(v1, a00);
	p2 = glm::dot(v1, a00);
	r = CalcR(extent, a01);
	if (Max(-Max(p0, p1, p2), Min(p0, p1, p2)) > r) return false;
	//Test the axis a02
	p0 = glm::dot(v0, a01);
	p1 = glm::dot(v1, a01);
	p2 = glm::dot(v1, a01);
	r = CalcR(extent, a01);
	if (Max(-Max(p0, p1, p2), Min(p0, p1, p2)) > r) return false;
	//Test the axes a10
	p0 = glm::dot(v0, a10);
	p1 = glm::dot(v1, a10);
	p2 = glm::dot(v1, a10);
	r = CalcR(extent, a10);
	if (Max(-Max(p0, p1, p2), Min(p0, p1, p2)) > r) return false;
	//Test the axes a11
	p0 = glm::dot(v0, a11);
	p1 = glm::dot(v1, a11);
	p2 = glm::dot(v1, a11);
	r = CalcR(extent, a11);
	if (Max(-Max(p0, p1, p2), Min(p0, p1, p2)) > r) return false;
	//Test the axes a12
	p0 = glm::dot(v0, a12);
	p1 = glm::dot(v1, a12);
	p2 = glm::dot(v1, a12);
	r = CalcR(extent, a12);
	if (Max(-Max(p0, p1, p2), Min(p0, p1, p2)) > r) return false;
	//Test the axes a20
	p0 = glm::dot(v0, a20);
	p1 = glm::dot(v1, a20);
	p2 = glm::dot(v1, a20);
	r = CalcR(extent, a20);
	if (Max(-Max(p0, p1, p2), Min(p0, p1, p2)) > r) return false;
	//Test the axes a21
	p0 = glm::dot(v0, a21);
	p1 = glm::dot(v1, a21);
	p2 = glm::dot(v1, a21);
	r = CalcR(extent, a21);
	if (Max(-Max(p0, p1, p2), Min(p0, p1, p2)) > r) return false;
	//Test the axes a22
	p0 = glm::dot(v0, a22);
	p1 = glm::dot(v1, a22);
	p2 = glm::dot(v1, a22);
	r = CalcR(extent, a22);
	if (Max(-Max(p0, p1, p2), Min(p0, p1, p2)) > r) return false;

	//Test the three axes corresponding to the face normals of AABB b (category 1)
	//Exit if
	if (Max(v0.x, v1.x, v2.x) < -extent || Min(v0.x, v1.x, v2.x) > extent) return false;

	glm::vec3 n = glm::cross(f0, f1);
	return TestPlane(n, glm::dot(n, v0), c, extent);
}

float AABB::CalcR(float extent, glm::vec3 a)
{
	return (extent * abs(glm::dot(u0, a)) + extent * abs(glm::dot(u1, a)) + extent * abs(glm::dot(u2, a)));
}

bool AABB::TestPlane(glm::vec3 normal, float planeD, glm::vec3 center, float extent)
{
	//Compute the projection interval radius of a b onto L(t) = b.c + t * p.n
	float r = extent * abs(normal.x) + extent * abs(normal.y) + extent * abs(normal.z);
	//Compute the distance of box center from plan
	float s = glm::dot(normal, center) - planeD;
	//Intersection occurs when distance s falls within [-r,+r] interval
	return abs(s) <= r;
}

float AABB::LargestSide(glm::vec3 & firstV, glm::vec3 & secondV, glm::vec3 & thirdV)
{
	float largest = glm::distance(firstV, secondV);
	float secondSide = glm::distance(secondV, thirdV);
	float thirdSide = glm::distance(thirdV, firstV);
	if (secondSide > largest)
		largest = secondSide;
	if (thirdSide > largest)
		largest = thirdSide;
	return largest;
}

