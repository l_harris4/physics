#ifndef AABB_HG
#define AABB_HG
#include "cGameObject.h"
#include "cTriangle.h"
#include <vector>
#include <glm\vec3.hpp>
#include <glm\glm.hpp>
#include "cVAOMeshManager.h"
#include <map>

class AABB
{
public:
	AABB();
	AABB(cGameObject* worldObject);
	~AABB();
	float calcGameObjectExtent(cGameObject* worldObject);
	float width;
	glm::vec3 minPoint;
	//std::vector<AABB> subBoxes;
	std::map<unsigned int, AABB> subBoxes;
	AABB* parent;
	std::vector<cPhysTriangle*> containedTriangles;
	void findContainedTriangles();
	void findContainedTriangles(std::vector<cPhysTriangle*>const& parentsTriangles);
	sVAOInfo* p_VAODrawInfo;
	glm::vec3 u0;
	glm::vec3 u1;
	glm::vec3 u2;
	int divisionNumber = 5;
	void FillOutAABB();
	bool TestTriangleAABB(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3);
	float CalcR(float extent, glm::vec3 a);
	bool TestPlane(glm::vec3 normal, float planeD, glm::vec3 center, float extent);
	float LargestSide(glm::vec3 &firstV, glm::vec3 &secondV, glm::vec3 &thirdV);
	
};
#endif // !AABB_HG

