#include "cShield.h"
#include "cTriangle.h"
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include "Utilities.h"	
#include "cCollision.h"
#include "Physics.h"
#include <iostream>
#include "cGameObjectMediator.h"
#include "cShip.h"

cShield::cShield()
{
	//set the default values
	this->radius = 0.8f;
	this->collidedThisFrame = false;
	this->editable = true;
	this->health = 3;
	this->hittingSphere = false;
	this->hittingGround = false;
}

cShield::~cShield()
{
}

void cShield::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
{
	//update its position based on the position of the ship
	glm::vec4 tempPos = glm::toMat4(theShip->modelData.qOrientation) * offSet;

	modelData.position = glm::vec3(tempPos.x + theShip->modelData.position.x
		, tempPos.y + theShip->modelData.position.y,
		tempPos.z + theShip->modelData.position.z);
	modelData.qOrientation = theShip->modelData.qOrientation;
}
