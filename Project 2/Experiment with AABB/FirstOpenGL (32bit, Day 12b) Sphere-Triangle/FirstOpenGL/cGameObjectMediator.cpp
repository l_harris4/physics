#include "cGameObjectMediator.h"
#include "cSphere.h"
#include "cShield.h"
#include "cShip.h"
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include <glm\vec4.hpp>
#include "cCollision.h"
#include "Physics.h"
#include "cShip.h"
#include <iostream>
#include "AABB.h"


extern cVAOMeshManager* g_pVAOManager;
extern cCollisionManager*  g_pCollisionManager;

//ctor
cGameObjectMediator::cGameObjectMediator()
{
}

//dtor
cGameObjectMediator::~cGameObjectMediator()
{
	//delete all the game objects
	for (int i = 0; i < vecGameObjects.size(); i++)
	{
		delete vecGameObjects[i];
	}
}

//move the main sphere
void cGameObjectMediator::MoveMainSphere(glm::vec3 force)
{
	((cSphere*)vecGameObjects[mainObjectIndex])->velocity += force;
}

void cGameObjectMediator::TurnShip(glm::vec3 turn)
{
	((cShip*)vecGameObjects[mainObjectIndex])->adjustQOrientationFormDeltaEuler(turn);
}

void cGameObjectMediator::ThrustShip(float thrust)
{
	((cShip*)vecGameObjects[mainObjectIndex])->decellerating = false;
	((cShip*)vecGameObjects[mainObjectIndex])->directedAcceleration.z += thrust;
}

void cGameObjectMediator::RemoveAccelShip()
{
	((cShip*)vecGameObjects[mainObjectIndex])->decellerating = true;
	((cShip*)vecGameObjects[mainObjectIndex])->directedAcceleration.z = 0;
}

void cGameObjectMediator::SetCameraAttributes(glm::vec3 & position, glm::vec3 & targetPosition)
{
	targetPosition = ((cShip*)vecGameObjects[mainObjectIndex])->modelData.position;

	glm::vec4 tempPos = glm::toMat4(((cShip*)vecGameObjects[mainObjectIndex])->modelData.qOrientation) *
		glm::vec4(0, 1, -10, 1);

	position = glm::vec3(tempPos.x + ((cShip*)vecGameObjects[mainObjectIndex])->modelData.position.x
		, tempPos.y + ((cShip*)vecGameObjects[mainObjectIndex])->modelData.position.y,
		tempPos.z + ((cShip*)vecGameObjects[mainObjectIndex])->modelData.position.z);

	//glm::vec4 tempPos = glm::toMat4(((cShip*)vecGameObjects[mainObjectIndex])->modelData.qOrientation) *
	//	glm::vec4(0 + ((cShip*)vecGameObjects[mainObjectIndex])->modelData.position.x,
	//		1 + ((cShip*)vecGameObjects[mainObjectIndex])->modelData.position.y,
	//		-10 + ((cShip*)vecGameObjects[mainObjectIndex])->modelData.position.z,
	//		1);
	//position = glm::vec3(tempPos.x
	//	, tempPos.y,
	//	tempPos.z);
}

//update all the game objects
void cGameObjectMediator::UpdateGameObjects(float deltaTime)
{
	int totalGameObjects = vecGameObjects.size();
	for (int index = 0; index != totalGameObjects; index++)
	{
		iGameObject* pCurGO = vecGameObjects[index];

		pCurGO->Update(&vecGameObjects, deltaTime);

	}//for ( int index...
}

void cGameObjectMediator::SetUpSpheres()
{

	int totalGameObjects = vecGameObjects.size();
	for (int index = 0; index != totalGameObjects; index++)
	{
		iGameObject* pCurGO = vecGameObjects[index];
		//this is where it is breaking
		if (pCurGO->GetType() == "sphere")
		{
			glm::vec3 tempOffset = (vecGameObjects[mainObjectIndex]->GetModelData()->position - pCurGO->GetModelData()->position);
			((cSphere*)pCurGO)->offSet = glm::vec4(tempOffset,1);
			((cSphere*)pCurGO)->theShip = (cShip*)(vecGameObjects[mainObjectIndex]);
		}
		else if (pCurGO->GetType() == "shield")
		{
			glm::vec3 tempOffset = (vecGameObjects[mainObjectIndex]->GetModelData()->position - pCurGO->GetModelData()->position);
			((cShield*)pCurGO)->offSet = glm::vec4(tempOffset, 1);
			((cShield*)pCurGO)->theShip = (cShip*)(vecGameObjects[mainObjectIndex]);
		}

	}//for ( int index...
}

unsigned int CalcId(glm::vec3 pos)
{
	const unsigned long long Z_MULT = 1;
	const unsigned long long Y_MULT = 100;
	const unsigned long long X_MULT = 10000;

	unsigned int ID =
		((int)(pos.x) * X_MULT) +		// ?xxxxx,------,------
		((int)(pos.y) * Y_MULT) +		// ------,?yyyyy,------
		((int)(pos.z)); //* Z_MULT;		// ------,------,?zzzzz

	return ID;
}

//find all colliding triangles for a specific sphere
bool cGameObjectMediator::GetCollidingTriangles(glm::vec3 objectPos, float objectRadius, iGameObject* piTriangleObject, std::vector<cPhysTriangle>& closestTriangles)
{
	cGameObject* pTriangleObject = (cGameObject*)piTriangleObject;
	//cSphere* pSphere = (cSphere*)piSphere;

	//calculate which A level box the sphere is in
	float AABBsizeA = mainBox->width / mainBox->divisionNumber;
	float AABBsizeB = (mainBox->width / mainBox->divisionNumber) / mainBox->divisionNumber;
	glm::vec3 position = objectPos;
	position -= mainBox->minPoint;
	glm::vec3 clampedPosition1 = position;
	clampedPosition1.x = ((floor(clampedPosition1.x / AABBsizeA)) * AABBsizeA) + mainBox->minPoint.x;
	clampedPosition1.y = ((floor(clampedPosition1.y / AABBsizeA)) * AABBsizeA) + mainBox->minPoint.y;
	clampedPosition1.z = ((floor(clampedPosition1.z / AABBsizeA)) * AABBsizeA) + mainBox->minPoint.z;
	glm::vec3 clampedPosition2 = position;
	clampedPosition2.x = ((floor(clampedPosition2.x / AABBsizeB)) * AABBsizeB) + mainBox->minPoint.x;
	clampedPosition2.y = ((floor(clampedPosition2.y / AABBsizeB)) * AABBsizeB) + mainBox->minPoint.y;
	clampedPosition2.z = ((floor(clampedPosition2.z / AABBsizeB)) * AABBsizeB) + mainBox->minPoint.z;
	unsigned int ID1 = CalcId(clampedPosition1);
	unsigned int ID2 = CalcId(clampedPosition2);

	//std::cout << "ID:" << ID1 << std::endl;
	currentBox = &mainBox->subBoxes[ID1].subBoxes[ID2];
	//currentBox = &mainBox->subBoxes[90809];


	int size = currentBox->containedTriangles.size();

	int activeIndex = -1;
	for (int triIndex = 0; triIndex != size; triIndex++)
	{
		cPhysTriangle curTriangle = (*currentBox->containedTriangles[triIndex]);

		//get the closest point on a triangle
		glm::vec3 theClosestPoint = curTriangle.ClosestPtPointTriangle(objectPos);
		//get the distance to that point
		float thisDistance = glm::distance(objectPos, theClosestPoint);
		//if the distance is less than the sphere radius, then a collision happened
		if (thisDistance < objectRadius)
		{
			closestTriangles.push_back(curTriangle);
		}

	}

	if (closestTriangles.size() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
	//cGameObject* pTriangleObject = (cGameObject*)piTriangleObject;
	//cSphere* pSphere = (cSphere*)piSphere;

	//sVAOInfo VAODrawInfo;
	//::g_pVAOManager->lookupVAOFromName(pTriangleObject->modelData.meshName, VAODrawInfo);
	//int numberOfTriangles = VAODrawInfo.vecPhysTris.size();

	//int activeIndex = -1;
	//for (int triIndex = 0; triIndex != numberOfTriangles; triIndex++)
	//{
	//	cPhysTriangle curTriangle = VAODrawInfo.vecPhysTris[triIndex];
	//	//////////////////////////////////
	//	//Below are the steps needed if the game object can be moved, not just a static model
	//	//at position 0
	//	if (pTriangleObject->editable) {
	//		glm::vec4 tempVec1(curTriangle.vertex[0].x, curTriangle.vertex[0].y, curTriangle.vertex[0].z, 1);
	//		tempVec1 = pTriangleObject->modelData.worldMatrix * tempVec1;

	//		glm::vec4 tempVec2(curTriangle.vertex[1].x, curTriangle.vertex[1].y, curTriangle.vertex[1].z, 1);
	//		tempVec2 = pTriangleObject->modelData.worldMatrix * tempVec2;

	//		glm::vec4 tempVec3(curTriangle.vertex[2].x, curTriangle.vertex[2].y, curTriangle.vertex[2].z, 1);
	//		tempVec3 = pTriangleObject->modelData.worldMatrix * tempVec3;

	//		curTriangle.vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
	//		curTriangle.vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
	//		curTriangle.vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
	//	}
	//	////////////////////////////////////////////////////////////////////////

	//	//get the closest point on a triangle
	//	glm::vec3 theClosestPoint = curTriangle.ClosestPtPointTriangle(pSphere->GetModelData()->position);
	//	//get the distance to that point
	//	float thisDistance = glm::distance(pSphere->GetModelData()->position, theClosestPoint);
	//	//if the distance is less than the sphere radius, then a collision happened
	//	if (thisDistance < pSphere->radius)
	//	{
	//		closestTriangles.push_back(curTriangle);
	//	}

	//}

	//if (closestTriangles.size() > 0)
	//{
	//	return true;
	//}
	//else
	//{
	//	return false;
	//}
}
//
////find all colliding triangles for a specific sphere
//bool cGameObjectMediator::GetCollidingTrianglesWithAABB(iGameObject* piSphere, std::vector<cPhysTriangle>& closestTriangles)
//{
//	//cGameObject* pTriangleObject = (cGameObject*)piTriangleObject;
//	cSphere* pSphere = (cSphere*)piSphere;
//
//	//we are gonna caluculate the position of the AABB in the vector
//	//because we havnt done hashing yet
//	glm::vec3 pos = pSphere->GetModelData()->position;
//	int xUnits = (int)(pos.x - mainBox->minPoint.x) / (int)(mainBox->width);
//	int yUnits = (int)(pos.y - mainBox->minPoint.y) / (int)(mainBox->width);
//	int zUnits = (int)(pos.z - mainBox->minPoint.z) / (int)(mainBox->width);
//	int totalIn = (xUnits * mainBox->divisionNumber *mainBox->divisionNumber) +
//		(yUnits *mainBox->divisionNumber) + zUnits;
//	return true;
//
//	//use the position to find what AABB the sphere is in then
//	//go through thr triangles of the AABB and see which ones the sphere is hitting
//
//	//sVAOInfo VAODrawInfo;
//	//::g_pVAOManager->lookupVAOFromName(pTriangleObject->modelData.meshName, VAODrawInfo);
//	//int numberOfTriangles = VAODrawInfo.vecPhysTris.size();
//
//	//int activeIndex = -1;
//	//for (int triIndex = 0; triIndex != numberOfTriangles; triIndex++)
//	//{
//	//	cPhysTriangle curTriangle = VAODrawInfo.vecPhysTris[triIndex];
//
//	//	//get the closest point on a triangle
//	//	glm::vec3 theClosestPoint = curTriangle.ClosestPtPointTriangle(pSphere->GetModelData()->position);
//	//	//get the distance to that point
//	//	float thisDistance = glm::distance(pSphere->GetModelData()->position, theClosestPoint);
//	//	//if the distance is less than the sphere radius, then a collision happened
//	//	if (thisDistance < pSphere->radius)
//	//	{
//	//		closestTriangles.push_back(curTriangle);
//	//	}
//
//	//}
//
//	//if (closestTriangles.size() > 0)
//	//{
//	//	return true;
//	//}
//	//else
//	//{
//	//	return false;
//	//}
//}

//compute all the interactions for a ship
void cGameObjectMediator::ComputeInteractions(cShip * ship)
{
	//reset some attributes
	ship->collidedThisFrame = false;
	ship->hittingGround = false;

	//create a new collision
	cCollision collision;
	collision.mainObject = ship;

	//rotate the sphere
	//sphere->modelData.orientation2.x -= 0.03;


	int size = vecGameObjects.size();

	//go through all the other game objects
	for (int indexEO = 0; indexEO != size; indexEO++)
	{
		// Don't test for myself
		if (ship == vecGameObjects[indexEO])
			continue;

		iGameObject* pOtherObject = vecGameObjects[indexEO];

		if (pOtherObject->GetType() == "plane") {

			float distance = 99999; //value way too big for our simulation

									//will have to find if multiple triangles were in range
			std::vector<cPhysTriangle> closestTriangles;
			if (GetCollidingTriangles(ship->modelData.position, ship->radius, pOtherObject, closestTriangles))
			{
				int trianglesSize = closestTriangles.size();
				//if there was a multiple triangle collision say so
				if (trianglesSize > 1)
				{
					std::cout << "Multi-Triangle Collision" << std::endl;
				}
				//push the triangle into the collision
				for (int i = 0; i < trianglesSize; i++)
				{
					std::pair<cPhysTriangle, iGameObject*> thePair;
					thePair = std::make_pair(closestTriangles[i], pOtherObject);
					collision.triangles.push_back(thePair);
				}
				ship->hittingGround = true;
			}
		}

	}


	ship->hittingSphere = ship->collidedThisFrame;

	::g_pCollisionManager->AddCollision(collision);

}

//compute all the interactions for a sphere
void cGameObjectMediator::ComputeInteractions(cSphere * sphere)
{
	//reset some attributes
	sphere->collidedThisFrame = false;
	sphere->hittingGround = false;

	//create a new collision
	cCollision collision;
	collision.mainObject = sphere;

	//rotate the sphere
	//sphere->modelData.orientation2.x -= 0.03;


	int size = vecGameObjects.size();

	//go through all the other game objects
	for (int indexEO = 0; indexEO != size; indexEO++)
	{
		// Don't test for myself
		if (sphere == vecGameObjects[indexEO])
			continue;

		iGameObject* pOtherObject = vecGameObjects[indexEO];

		if (pOtherObject->GetType() == "plane") {

			float distance = 99999; //value way too big for our simulation

									//will have to find if multiple triangles were in range
			std::vector<cPhysTriangle> closestTriangles;
			if (GetCollidingTriangles(sphere->modelData.position, sphere->radius, pOtherObject, closestTriangles))
			{
				int trianglesSize = closestTriangles.size();
				//if there was a multiple triangle collision say so
				if (trianglesSize > 1)
				{
					std::cout << "Multi-Triangle Collision" << std::endl;
				}
				//push the triangle into the collision
				for (int i = 0; i < trianglesSize; i++)
				{
					std::pair<cPhysTriangle, iGameObject*> thePair;
					thePair = std::make_pair(closestTriangles[i], pOtherObject);
					collision.triangles.push_back(thePair);
				}
				sphere->hittingGround = true;
			}
		}

	}


	sphere->hittingSphere = sphere->collidedThisFrame;

	::g_pCollisionManager->AddCollision(collision);

}

void cGameObjectMediator::LoadAABB(AABB * theBox)
{
	theBox = new AABB((cGameObject*)(vecGameObjects[0]));
	mainBox = theBox;
	currentBox = mainBox;
}

glm::vec3 cGameObjectMediator::GetHitPosition()
{
	return ((cShip*)vecGameObjects[mainObjectIndex])->GetHitPosition();
}
