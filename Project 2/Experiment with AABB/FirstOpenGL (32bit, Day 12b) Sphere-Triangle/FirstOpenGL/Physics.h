#ifndef _Physics_HG_
#define _Physics_HG_

//#include "cGameObject.h"
#include <glm\vec3.hpp>
#include "cTriangle.h"
#include <vector>
class iGameObject;

// testing if spheres are penetrating eachother
bool PenetrationTestSphereSphere( iGameObject* pA, iGameObject* pB );
// calculate bounce velocity based on other spheres
void BounceCalcSphereSpheres(iGameObject* pA, std::vector<iGameObject*> otherObjects);
// calculate reflecting velocity based on triangles it is colliding with
void calcTrianglesSphereCollision(iGameObject* sphereObject, std::vector<std::pair<cPhysTriangle, iGameObject*>> &theTriangles);



#endif
