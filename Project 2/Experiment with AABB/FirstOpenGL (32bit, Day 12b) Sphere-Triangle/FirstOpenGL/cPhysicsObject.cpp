#include "cPhysicsObject.h"
#include <glm/vec4.hpp>

cPhysicsObject::cPhysicsObject()
{
	this->velocity = glm::vec3(0.0f);
	this->acceleration = glm::vec3(0.0f);
	this->directedVelocity = glm::vec4(0.0f);
	this->directedAcceleration = glm::vec3(0.0f);
	
}

cPhysicsObject::~cPhysicsObject()
{
}
