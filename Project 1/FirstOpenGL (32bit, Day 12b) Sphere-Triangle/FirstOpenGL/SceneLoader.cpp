// This file is used to laod the models
#include "cGameObject.h"
#include <vector>
#include "Utilities.h"		// getRandInRange()
#include <glm/glm.hpp>
#include "cLightManager.h"
#include <fstream>

extern std::vector< cGameObject* >  g_vecGameObjects;
extern cGameObject* g_pTheDebugSphere;
extern cLightManager*		g_pLightManager;


//const float SURFACEOFGROUND = -10.0f;
//const float RIGHTSIDEWALL = 15.0f;
//const float LEFTSIDEWALL = -15.0f;

bool LoadModelsLightsFromFile()
{
	std::string filename = "ObjectsConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	int lightIndex = -1;

	try {
		while (getline(inFile, line))
		{
			if (line == "")
			{
				continue;
			}
			if (line == "Object Start") {
				cGameObject* pTempGO = new cGameObject();
				pTempGO->bIsUpdatedInPhysics = false;
				pTempGO->typeOfObject = eTypeOfObject::SPHERE;
				pTempGO->radius = 1.0f;
				::g_vecGameObjects.push_back(pTempGO);
				continue;
			}
			if (line == "Light Start") {
				::g_pLightManager->CreateLights(1, true);
				lightIndex++;
				continue;
			}
			//TODO add type of object eTypeOfObject::PLANE;

			if (line.find("type:") != std::string::npos) {
				line.replace(0, 6, "");
				if (line == "plane")
				{
					::g_vecGameObjects.back()->typeOfObject = eTypeOfObject::PLANE;
				}
				else if (line == "cube")
				{
					::g_vecGameObjects.back()->typeOfObject = eTypeOfObject::CUBE;
				}
				else if (line == "sphere")
				{
					::g_vecGameObjects.back()->typeOfObject = eTypeOfObject::SPHERE;
				}
				continue;
			}

			if (line.find("gravity:") != std::string::npos) {
				line.replace(0, 9, "");
				::g_vecGameObjects.back()->affectedByGravity = line == "true";
				continue;
			}

			if (line.find("editable:") != std::string::npos) {
				line.replace(0, 10, "");
				::g_vecGameObjects.back()->editable = line == "true";
				continue;
			}

			if (line.find("filename:") != std::string::npos) {
				line.replace(0, 10, "");
				::g_vecGameObjects.back()->meshName = line;
				continue;
			}
			if (line.find("physics:") != std::string::npos) {
				line.replace(0, 9, "");
				::g_vecGameObjects.back()->bIsUpdatedInPhysics = line == "true";
				continue;
			}
			if (line.find("wireframe:") != std::string::npos) {
				line.replace(0, 11, "");
				::g_vecGameObjects.back()->bIsWireFrame = line == "true";
				continue;
			}
			if (line.find("velocity:") != std::string::npos) {
				line.replace(0, 10, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_vecGameObjects.back()->vel.x = stof(number);
						number = "";
					}
					else
					{
						::g_vecGameObjects.back()->vel.y = stof(number);
						number = "";
					}
				}
				::g_vecGameObjects.back()->vel.z = stof(number);
				continue;
			}
			if (line.find("position: ") != std::string::npos) {
				line.replace(0, 10, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_vecGameObjects.back()->position.x = stof(number);
						number = "";
					}
					else
					{
						::g_vecGameObjects.back()->position.y = stof(number);
						number = "";
					}
				}
				::g_vecGameObjects.back()->position.z = stof(number);
				continue;
			}
			if (line.find("orientation: ") != std::string::npos) {
				line.replace(0, 13, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_vecGameObjects.back()->orientation2.x = glm::radians(stof(number));
						number = "";
					}
					else
					{
						::g_vecGameObjects.back()->orientation2.y = glm::radians(stof(number));
						number = "";
					}
				}
				::g_vecGameObjects.back()->orientation2.z = glm::radians(stof(number));
				continue;
			}
			if (line.find("scale:") != std::string::npos) {
				line.replace(0, 7, "");
				::g_vecGameObjects.back()->scale = stof(line);
				continue;
			}
			if (line.find("colour:") != std::string::npos) {
				line.replace(0, 8, "");

				std::string number;
				bool rValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (rValue)
					{
						rValue = false;
						::g_vecGameObjects.back()->diffuseColour.x = stof(number);
						number = "";
					}
					else
					{
						::g_vecGameObjects.back()->diffuseColour.y = stof(number);
						number = "";
					}
				}
				::g_vecGameObjects.back()->diffuseColour.z = stof(number);
				::g_vecGameObjects.back()->diffuseColour.a = 1.0f;
				::g_vecGameObjects.back()->originalColour = ::g_vecGameObjects.back()->diffuseColour;
				continue;
				//change the times for the most recent media object
			}
			if (line.find("filename_l:") != std::string::npos) {
				line.replace(0, 12, "");
				::g_pLightManager->vecLights[lightIndex].fileName = line;
				cGameObject* pTempGO = new cGameObject();
				pTempGO->position = ::g_pLightManager->vecLights[lightIndex].position;
				::g_pLightManager->vecLights[lightIndex].gameObjectIndex = ::g_vecGameObjects.size();
				pTempGO->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
				pTempGO->meshName = line;
				pTempGO->typeOfObject = eTypeOfObject::SPHERE;
				pTempGO->bIsUpdatedInPhysics = false;
				pTempGO->scale = 1;
				pTempGO->radius = 1.0f;
				::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
				continue;
			}
			if (line.find("attentuation:") != std::string::npos) {
				line.replace(0, 14, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].attenuation.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].attenuation.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].attenuation.z = stof(number);
				continue;
			}
			if (line.find("position_l:") != std::string::npos) {
				line.replace(0, 12, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].position.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].position.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].position.z = stof(number);
				continue;
				//change the times for the most recent media object
			}
			if (line.find("colour_l:") != std::string::npos) {
				line.replace(0, 10, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].diffuse.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].diffuse.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].diffuse.z = stof(number);
				continue;
				//change the times for the most recent media object
			}
		}
		{// STARTOF: Add the debug sphere
			::g_pTheDebugSphere = new cGameObject();
			::g_pTheDebugSphere->scale = 1.0f;
			::g_pTheDebugSphere->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			::g_pTheDebugSphere->meshName = "SphereRadius1";
			::g_pTheDebugSphere->typeOfObject = eTypeOfObject::SPHERE;
			::g_pTheDebugSphere->radius = 1.0f;
			::g_pTheDebugSphere->bIsUpdatedInPhysics = false;
			// NOTE: I'm NOT adding it to the vector of objects
			//::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
		}// ENDOF: Add the debug sphere

	}
	catch (std::exception ex)
	{
		return false;
	}
	return true;
}


//
//void LoadModelsIntoScene(void)
//{
//	{	// Right side plane
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
//		pTempGO->meshName = "Level1";		// Was teapot
//												// ***
//		pTempGO->bIsUpdatedInPhysics = false;
//		pTempGO->bIsWireFrame = true;
//		//pTempGO->editable = true;
//		pTempGO->typeOfObject = eTypeOfObject::PLANE;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//
//
//	{	// Right side plane
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
//		pTempGO->meshName = "Platform";		// Was teapot
//		// ***
//		pTempGO->bIsUpdatedInPhysics = false;
//		pTempGO->bIsWireFrame = true;
//		pTempGO->typeOfObject = eTypeOfObject::PLANE;
//		// ***
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//	{	// left side plane
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
//		pTempGO->meshName = "LeftWall";		// Was teapot
//		// ***
//		pTempGO->bIsUpdatedInPhysics = false;
//		pTempGO->bIsWireFrame = true;
//		pTempGO->typeOfObject = eTypeOfObject::PLANE;
//		// ***
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.y = 4.0f;
//		pTempGO->position.x = -4.0f;
//		pTempGO->vel.x = 2.0f;
//		pTempGO->vel.y = 1.0f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->radius = 1.0f;
//		pTempGO->affectedByGravity = true;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{	// Right side plane
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
//		pTempGO->meshName = "Cube";		// Was teapot
//											// ***
//		pTempGO->vel.x = -3;
//		pTempGO->bIsUpdatedInPhysics = true;
//		pTempGO->bIsWireFrame = true;
//		pTempGO->editable = true;
//		pTempGO->typeOfObject = eTypeOfObject::CUBE;
//		// ***
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{	// Right side plane
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->scale = 1.0f;
//		pTempGO->position.z = -10;
//		pTempGO->position.x = -50;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
//		pTempGO->meshName = "Cube";		// Was teapot
//		pTempGO->vel.x = 3;
//		// ***
//		pTempGO->bIsUpdatedInPhysics = true;
//		pTempGO->bIsWireFrame = true;
//		pTempGO->editable = true;
//		pTempGO->typeOfObject = eTypeOfObject::CUBE;
//		// ***
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{	// Right side plane
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->scale = 1.0f;
//		pTempGO->position.z = -30;
//		pTempGO->position.x = -20;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
//		pTempGO->meshName = "Cube";		// Was teapot
//		pTempGO->vel.x = 3;
//		// ***
//		pTempGO->bIsUpdatedInPhysics = true;
//		pTempGO->bIsWireFrame = true;
//		pTempGO->editable = true;
//		pTempGO->typeOfObject = eTypeOfObject::CUBE;
//		// ***
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = -20.5f;
//		pTempGO->position.y = 4.0f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = -0.5f;
//		pTempGO->position.y = 4.0f;
//		pTempGO->position.z = -24.0f;
//		pTempGO->vel.x = -3.3f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = -10.5f;
//		pTempGO->position.y = 4.0f;
//		pTempGO->position.z = -24.0f;
//		pTempGO->vel.z = +3.3f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = -10.5f;
//		pTempGO->position.y = 3.0f;
//		pTempGO->position.z = -28.0f;
//		pTempGO->vel.z = +3.3f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = -10.5f;
//		pTempGO->position.y = 3.0f;
//		pTempGO->position.z = -34.0f;
//		pTempGO->vel.x = +3.3f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = +10.5f;
//		pTempGO->position.y = 3.0f;
//		pTempGO->position.z = -34.0f;
//		pTempGO->vel.x = +3.3f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = -20.5f;
//		pTempGO->position.y = 3.0f;
//		pTempGO->position.z = -34.0f;
//		pTempGO->vel.z = -3.3f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = -10.5f;
//		pTempGO->position.y = 3.0f;
//		pTempGO->position.z = -54.0f;
//		pTempGO->vel.x = +3.3f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//
//	{
//		cGameObject* pTempGO = new cGameObject();
//		pTempGO->position.x = -20.5f;
//		pTempGO->position.y = 3.0f;
//		pTempGO->position.z = -54.0f;
//		pTempGO->vel.z = +3.3f;
//		pTempGO->scale = 1.0f;
//		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//		pTempGO->originalColour = pTempGO->diffuseColour;
//		pTempGO->meshName = "SphereRadius1";			// was dolphin
//		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//		pTempGO->affectedByGravity = true;
//		pTempGO->radius = 1.0f;
//		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
//	}
//	// Add a bunch of spheres....
//	//for ( int count = 0; count != 8; count++ )
//	//{
//	//	cGameObject* pTempGO = new cGameObject();
//	//	pTempGO->position.x = getRandInRange<float>(-20.0f, 7.0f);	// -10 to 10
//	//	pTempGO->position.y = getRandInRange<float>( 1.0f, 10.0f ); // -2 to Infinity
//	//	pTempGO->vel.x = getRandInRange<float>( -3.0f, 3.0f );
//	//	pTempGO->vel.y = getRandInRange<float>( -1.0f, 2.0f );
//	//	pTempGO->vel.z = getRandInRange<float>(-1.0f, 2.0f);
//	//	pTempGO->scale = 1.0f;
//	//	pTempGO->diffuseColour = glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );
//	//	pTempGO->meshName = "SphereRadius1";
//	//	pTempGO->affectedByGravity = true;
//	//	pTempGO->typeOfObject = eTypeOfObject::SPHERE;
//	//	pTempGO->radius = 1.0f;	
//	//	::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
//	//}// for ( int count...
//
//
//	// Set the objects to random colours
//	for (int index = 0; index != ::g_vecGameObjects.size(); index++)
//	{
//		::g_vecGameObjects[index]->diffuseColour.r = getRandInRange(0.0f, 1.0f);
//		::g_vecGameObjects[index]->diffuseColour.g = getRandInRange(0.0f, 1.0f);
//		::g_vecGameObjects[index]->diffuseColour.b = getRandInRange(0.0f, 1.0f);
//	}
//
//	//	// Add a bunch more rabbits
//	//	const float SIZEOFWORLD = 6.0f;	
//	////	for ( int index = 3; index < MAXNUMBEROFGAMEOBJECTS; index++ )
//	//	for ( int index = 3; index < 100; index++ )
//	//	{
//	//		cGameObject* pTempGO = new cGameObject();
//	//		pTempGO->position.x = getRandInRange<float>(-SIZEOFWORLD, SIZEOFWORLD );
//	//		pTempGO->position.y = getRandInRange<float>(-SIZEOFWORLD, SIZEOFWORLD );
//	//		pTempGO->position.z = getRandInRange<float>(-SIZEOFWORLD, SIZEOFWORLD );
//	//		//::g_GameObjects[index]->scale = getRandInRange<float>( 7.0f, 15.0f );
//	//		// Pick a random colour for this bunny
//	//		pTempGO->diffuseColour.r = getRandInRange<float>(0.0f, 1.0f );
//	//		pTempGO->diffuseColour.g = getRandInRange<float>(0.0f, 1.0f );
//	//		pTempGO->diffuseColour.b = getRandInRange<float>(0.0f, 1.0f );
//	//		pTempGO->meshName = "bunny";
//	//		::g_vecGameObjects.push_back( pTempGO );
//	//	}
//
//
//		// Add the debug sphere
//	{// STARTOF: Add the debug sphere
//		::g_pTheDebugSphere = new cGameObject();
//		::g_pTheDebugSphere->scale = 1.0f;
//		::g_pTheDebugSphere->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
//		::g_pTheDebugSphere->meshName = "SphereRadius1";
//		::g_pTheDebugSphere->typeOfObject = eTypeOfObject::SPHERE;
//		::g_pTheDebugSphere->radius = 1.0f;
//		::g_pTheDebugSphere->bIsUpdatedInPhysics = false;
//		// NOTE: I'm NOT adding it to the vector of objects
//		//::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
//	}// ENDOF: Add the debug sphere
//
//
//	return;
//}
