#include "ModelUtilities.h" 
#include "cVAOMeshManager.h"
#include "cMesh.h"
#include <sstream>


extern cMesh g_MeshFractalTerrain;

void ReadFileToToken( std::ifstream &file, std::string token )
{
	bool bKeepReading = true;
	std::string garbage;
	do
	{
		file >> garbage;		// Title_End??
		if ( garbage == token )
		{
			return;
		}
	}while( bKeepReading );
	return;
}



// Takes a file name, loads a mesh
bool LoadPlyFileIntoMesh( std::string filename, cMesh &theMesh )
{
	// Load the vertices
	// c_str() changes a string to a "c style char* string"
	std::ifstream plyFile( filename.c_str() );

	if ( ! plyFile.is_open() )
	{	// Didn't open file, so return
		return false;
	}
	// File is open, let's read it

	ReadFileToToken( plyFile, "vertex" );
//	int numVertices = 0;
	plyFile >> theMesh.numberOfVertices;

	ReadFileToToken( plyFile, "face" );
//	int numTriangles = 0;
	plyFile >> theMesh.numberOfTriangles;		

	ReadFileToToken( plyFile, "end_header" );

	// Allocate the appropriate sized array (+a little bit)
	theMesh.pVertices = new cVertex_xyz_rgb_n[theMesh.numberOfVertices];
	theMesh.pTriangles = new cTriangle[theMesh.numberOfTriangles];

	// Read vertices
	for ( int index = 0; index < theMesh.numberOfVertices; index++ )
	{
		//end_header
		//-0.0312216 0.126304 0.00514924 0.850855 0.5 		
		float x, y, z, confidence, intensity;

		plyFile >> x;
		plyFile >> y;
		plyFile >> z;
//		plyFile >> confidence;
//		plyFile >> intensity;

		theMesh.pVertices[index].x = x;	// vertices[index].x = x;
		theMesh.pVertices[index].y = y;	// vertices[index].y = y;
		theMesh.pVertices[index].z = z; 
		theMesh.pVertices[index].r = 1.0f;	// vertices[index].g = 1.0f;
		theMesh.pVertices[index].g = 1.0f;	// vertices[index].b = 1.0f;
		theMesh.pVertices[index].b = 1.0f;	// vertices[index].r = 1.0f;
	}

	// Load the triangle (or face) information, too
	for ( int count = 0; count < theMesh.numberOfTriangles; count++ )
	{
		// 3 164 94 98 
		int discard = 0;
		plyFile >> discard;									// 3
		plyFile >> theMesh.pTriangles[count].vertex_ID_0;	// 164
		plyFile >> theMesh.pTriangles[count].vertex_ID_1;	// 94
		plyFile >> theMesh.pTriangles[count].vertex_ID_2;	// 98
	}

	theMesh.CalculateNormals();

	return true;
}

// Takes a file name, loads a mesh
bool LoadPlyFileIntoMeshWithNormals( std::string filename, cMesh &theMesh )
{
	// Load the vertices
	// c_str() changes a string to a "c style char* string"
	std::ifstream plyFile( filename.c_str() );

	if ( ! plyFile.is_open() )
	{	// Didn't open file, so return
		return false;
	}
	// File is open, let's read it

	ReadFileToToken( plyFile, "vertex" );
//	int numVertices = 0;
	plyFile >> theMesh.numberOfVertices;

	ReadFileToToken( plyFile, "face" );
//	int numTriangles = 0;
	plyFile >> theMesh.numberOfTriangles;		

	ReadFileToToken( plyFile, "end_header" );

	// Allocate the appropriate sized array (+a little bit)
	theMesh.pVertices = new cVertex_xyz_rgb_n[theMesh.numberOfVertices];
	theMesh.pTriangles = new cTriangle[theMesh.numberOfTriangles];

	// Read vertices
	for ( int index = 0; index < theMesh.numberOfVertices; index++ )
	{
		//end_header
		//-0.0312216 0.126304 0.00514924 0.850855 0.5 		
		float x, y, z, nx, ny, nz, confidence, intensity;

		plyFile >> x;
		plyFile >> y;
		plyFile >> z;
		plyFile >> nx >> ny >> nz;

		theMesh.pVertices[index].x = x;	// vertices[index].x = x;
		theMesh.pVertices[index].y = y;	// vertices[index].y = y;
		theMesh.pVertices[index].z = z; 
		theMesh.pVertices[index].r = 1.0f;	// vertices[index].g = 1.0f;
		theMesh.pVertices[index].g = 1.0f;	// vertices[index].b = 1.0f;
		theMesh.pVertices[index].b = 1.0f;	// vertices[index].r = 1.0f;
		theMesh.pVertices[index].nx = nx;	// vertices[index].g = 1.0f;
		theMesh.pVertices[index].ny = ny;	// vertices[index].b = 1.0f;
		theMesh.pVertices[index].nz = nz;	// vertices[index].r = 1.0f;
	}

	// Load the triangle (or face) information, too
	for ( int count = 0; count < theMesh.numberOfTriangles; count++ )
	{
		// 3 164 94 98 
		int discard = 0;
		plyFile >> discard;									// 3
		plyFile >> theMesh.pTriangles[count].vertex_ID_0;	// 164
		plyFile >> theMesh.pTriangles[count].vertex_ID_1;	// 94
		plyFile >> theMesh.pTriangles[count].vertex_ID_2;	// 98
	}

//	theMesh.CalculateNormals();

	return true;
}

//This will find mesh names in a text file, currently ObjectsConfig.txt and load the appropriate meshes
//into the VAOMeshManager
bool Load3DModelsIntoMeshManager(int shaderID, cVAOMeshManager* pVAOManager, std::string &error)
{

	std::string filename = "ObjectsConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	std::stringstream ssError;
	bool bAnyErrors = false;
	bool normals = false;

	try {
		while (getline(inFile, line))
		{
			if (line == "")
			{
				continue;
			}
			if (line.find("normals:") != std::string::npos) {
				line.replace(0, 9, "");
				normals = line == "true";
				continue;
			}
			if (line.find("filename_l:") != std::string::npos) {
				line.replace(0, 12, "");
				cMesh testMesh;
				testMesh.name = line;
				if (normals) {
					if (!LoadPlyFileIntoMeshWithNormals(line, testMesh))
					{
						//std::cout << "Didn't load model" << std::endl;
						ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
						bAnyErrors = true;
					}
				}
				else
				{
					if (!LoadPlyFileIntoMesh(line, testMesh))
					{
						//std::cout << "Didn't load model" << std::endl;
						ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
						bAnyErrors = true;
					}
				}
				if (!pVAOManager->loadMeshIntoVAO(testMesh, shaderID))
				{
					//std::cout << "Could not load mesh into VAO" << std::endl;
					ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
					bAnyErrors = true;
				}
				continue;
			}
			if (line.find("filename:") != std::string::npos) {
				line.replace(0, 10, "");
				cMesh testMesh;
				testMesh.name = line;
				if (normals) {
					if (!LoadPlyFileIntoMeshWithNormals(line, testMesh))
					{
						//std::cout << "Didn't load model" << std::endl;
						ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
						bAnyErrors = true;
					}
				}
				else
				{
					if (!LoadPlyFileIntoMesh(line, testMesh))
					{
						//std::cout << "Didn't load model" << std::endl;
						ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
						bAnyErrors = true;
					}
				}
				if (!pVAOManager->loadMeshIntoVAO(testMesh, shaderID))
				{
					//std::cout << "Could not load mesh into VAO" << std::endl;
					ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
					bAnyErrors = true;
				}
				continue;
			}
		}

	}
	catch (...)
	{
		return true;
	}
	if (!bAnyErrors)
	{
		// Copy the error string stream into the error string that
		//	gets "returned" (through pass by reference)
		error = ssError.str();
	}
	return bAnyErrors;

}
//
//
//bool Load3DModelsIntoMeshManager(int shaderID, cVAOMeshManager* pVAOManager, std::string &error)
//{
//
//
//	std::stringstream ssError;
//	bool bAnyErrors = false;
//
//	{
//		cMesh testMesh;
//		testMesh.name = "bunny";
//		if ( ! LoadPlyFileIntoMesh( "bun_zipper_res2_xyz.ply", testMesh ) )
//		{ 
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if ( ! pVAOManager->loadMeshIntoVAO( testMesh, shaderID ) )
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//	}
//	{
//		cMesh testMesh;
//		testMesh.name = "teapot";
//		if ( ! LoadPlyFileIntoMesh( "Utah_Teapot_1Unit_xyz.ply", testMesh ) )
//		{ 
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if ( ! pVAOManager->loadMeshIntoVAO( testMesh, shaderID ) )
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//	}	
//	{
//		cMesh testMesh;
//		testMesh.name = "dolphin";
//		if ( ! LoadPlyFileIntoMesh( "dolphin_xyz.ply", testMesh ) )
//		{ 
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if ( ! pVAOManager->loadMeshIntoVAO( testMesh, shaderID ) )
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//	}	
//	{
//		cMesh testMesh;
//		testMesh.name = "PlaneXZ";
//		if ( !LoadPlyFileIntoMeshWithNormals( "Flat_XZ_Plane_xyz.ply", testMesh ) )
//		{ 
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if ( ! pVAOManager->loadMeshIntoVAO( testMesh, shaderID ) )
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//	}	
//	{
//		cMesh testMesh;
//		testMesh.name = "SphereRadius1";
////		if ( ! LoadPlyFileIntoMesh( "Sphereply_xyz.ply", testMesh ) )
//		if ( ! LoadPlyFileIntoMeshWithNormals( "Sphereply_xyz_n.ply", testMesh ) )
//		{ 
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if ( ! pVAOManager->loadMeshIntoVAO( testMesh, shaderID ) )
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//	}	
//
//	// This is our Fractal terrain model
//	{
//		//extern cMesh g_MeshFractalTerrain;
//		cMesh testMesh;
//		testMesh.name = "Cube";
//		if (!LoadPlyFileIntoMeshWithNormals("CubePlatform.ply", testMesh))
//			//if ( ! LoadPlyFileIntoMeshWithNormals( "TestFloor.ply", g_MeshFractalTerrain ) )
//		{
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if (!pVAOManager->loadMeshIntoVAO(testMesh, shaderID))
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//
//	}	// ENDOF: load models
//
//	// This is our Fractal terrain model
//	{
//		//extern cMesh g_MeshFractalTerrain;
//		cMesh testMesh;
//		testMesh.name = "Platform";
//		if (!LoadPlyFileIntoMeshWithNormals("Platform.ply", testMesh))
//			//if ( ! LoadPlyFileIntoMeshWithNormals( "TestFloor.ply", g_MeshFractalTerrain ) )
//		{
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if (!pVAOManager->loadMeshIntoVAO(testMesh, shaderID))
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//
//	}	// ENDOF: load models
//		// This is our Fractal terrain model
//	{
//		//extern cMesh g_MeshFractalTerrain;
//		cMesh testMesh;
//		testMesh.name = "LeftWall";
//		if (!LoadPlyFileIntoMeshWithNormals("LeftWall.ply", testMesh))
//			//if ( ! LoadPlyFileIntoMeshWithNormals( "TestFloor.ply", g_MeshFractalTerrain ) )
//		{
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if (!pVAOManager->loadMeshIntoVAO(testMesh, shaderID))
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//
//	}	// ENDOF: load models
//		// This is our Fractal terrain model
//	{
//		//extern cMesh g_MeshFractalTerrain;
//		cMesh testMesh;
//		testMesh.name = "Level1";
//		if (!LoadPlyFileIntoMeshWithNormals("BallLevel1b.ply", testMesh))
//			//if ( ! LoadPlyFileIntoMeshWithNormals( "TestFloor.ply", g_MeshFractalTerrain ) )
//		{
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if (!pVAOManager->loadMeshIntoVAO(testMesh, shaderID))
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//
//	}	// ENDOF: load models
//
//	// This is our Fractal terrain model
//	{
//		//extern cMesh g_MeshFractalTerrain;
//		cMesh testMesh;
//		testMesh.name = "FractalTerrain";
//		if (!LoadPlyFileIntoMeshWithNormals("SlantedTestFloor.ply", testMesh))
//		//if ( ! LoadPlyFileIntoMeshWithNormals( "TestFloor.ply", g_MeshFractalTerrain ) )
//		{ 
//			//std::cout << "Didn't load model" << std::endl;
//			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
//			bAnyErrors = true;
//		}
//		if ( ! pVAOManager->loadMeshIntoVAO(testMesh, shaderID ) )
//		{
//			//std::cout << "Could not load mesh into VAO" << std::endl;
//			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
//			bAnyErrors = true;
//		}
//
//	}	// ENDOF: load models
//	if ( ! bAnyErrors ) 
//	{
//		// Copy the error string stream into the error string that
//		//	gets "returned" (through pass by reference)
//		error = ssError.str();
//	}
//
//	return bAnyErrors;
//}