#include "cCollisionManager.h"
#include "cCollision.h"
#include "cTriangle.h"

cCollisionManager::cCollisionManager()
{
	//this->addingIndex = 0;
}

cCollisionManager::~cCollisionManager()
{
}

void cCollisionManager::CalculateCollisions()
{
	int collisionsSize = collisions.size();
	for (int collisionIndex = 0; collisionIndex < collisionsSize; collisionIndex++)
	{
		collisions[collisionIndex].calcCollision();
	}
}

void cCollisionManager::AddCollision(cCollision collisionData)
{
	collisions.push_back(collisionData);
}

void cCollisionManager::ClearCollisions()
{
	collisions.clear();
	//this->addingIndex = 0;
}
