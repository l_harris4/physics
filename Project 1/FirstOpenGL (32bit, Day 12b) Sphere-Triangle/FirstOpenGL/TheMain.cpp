#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>			// C++ cin, cout, etc.
//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr


#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>

#include <vector>		//  smart array, "array" in most languages
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include "cCollision.h"

#include "Physics.h"	// Physics collision detection functions

#include "cLightManager.h"

// Forward declaration of the function
void DrawObject(cGameObject* pTheGO);

//// HACK plane collision detection and response
//const float SURFACEOFGROUND = -10.0f;
//const float RIGHTSIDEWALL = 15.0f;
//const float LEFTSIDEWALL = -15.0f;


// This is the function "signature" for the 
//	function WAY at the bottom of the file.
void PhysicsStep(double deltaTime);

//void LoadModelsIntoScene(void);
bool LoadModelsLightsFromFile();
bool Load3DModelsIntoMeshManager(int shaderID, cVAOMeshManager* pVAOManager, std::string &error);

// Used by the light drawing thingy
// Will draw a wireframe sphere at this location with this colour
void DrawDebugSphere(glm::vec3 location, glm::vec4 colour, float scale);
cGameObject* g_pTheDebugSphere;

//	static const int MAXNUMBEROFGAMEOBJECTS = 10;
//	cGameObject* g_GameObjects[MAXNUMBEROFGAMEOBJECTS];

// Remember to #include <vector>...
std::vector< cGameObject* >  g_vecGameObjects;


glm::vec3 g_cameraXYZOrig = glm::vec3(0.0f, 51.0f, 122.0f);	// 5 units "down" z
glm::vec3 g_cameraTarget_XYZ = glm::vec3(0.0f, 0.0f, 0.0f);
bool g_cameraOnBall = true;

cVAOMeshManager* g_pVAOManager = 0;		// or NULL, or nullptr

//cShaderManager	g_ShaderManager;		// Stack (no new)
cShaderManager*		g_pShaderManager;		// Heap, new (and delete)
cLightManager*		g_pLightManager;
cCollisionManager*  g_pCollisionManager;

bool g_bDrawDebugLightSpheres = false;

// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
							// w = shininess of the 
GLint uniLoc_bIsDebugWireFrameObject = -1;

GLint uniLoc_eyePosition = -1;	// Camera position
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

bool bIsShiftPressed(int mods);
bool bIsCtrlPressed(int mods);
bool bIsAltPressed(int mods);
bool bIsShiftPressedAlone(int mods);
bool bIsCtrlPressedAlone(int mods);
bool bIsAltPressedAlone(int mods);

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	if (key == GLFW_KEY_SPACE)
	{
		//		::g_GameObjects[1]->position.y += 0.01f;
		::g_vecGameObjects[1]->position.y += 0.01f;
	}

	const float CAMERASPEED = 0.25f;
	switch (key)
	{
	case GLFW_KEY_A:		// Left
		if (action == GLFW_REPEAT)
			::g_vecGameObjects[3]->vel.x -= 0.8;
		break;
	case GLFW_KEY_D:		// Right
		if (action == GLFW_REPEAT)
			::g_vecGameObjects[3]->vel.x += 0.8;
		break;
	case GLFW_KEY_W:		// Forward (along z)
		if (action == GLFW_REPEAT)
			::g_vecGameObjects[3]->vel.z -= 0.8;
		break;
	case GLFW_KEY_S:		// Backwards (along z)
		if (action == GLFW_REPEAT)
			::g_vecGameObjects[3]->vel.z += 0.8;
		break;
	case GLFW_KEY_UP:		// "Up" (along y axis)
		::g_vecGameObjects[0]->position.y += 0.1;
		break;
	case GLFW_KEY_SPACE:
		if (action == GLFW_PRESS) {
			::g_vecGameObjects[3]->vel.y += 4.0;
		}
		break;

	}// switch ( key )


	// Press SHIFT to change the lighting parameters
	if (bIsShiftPressedAlone(mods))
	{
		switch (key)
		{
		case GLFW_KEY_A:		// Left
			::g_pLightManager->vecLights[0].position.x -= CAMERASPEED;
			break;
		case GLFW_KEY_D:		// Right
			::g_pLightManager->vecLights[0].position.x += CAMERASPEED;
			break;
		case GLFW_KEY_W:		// Forward (along z)
			::g_pLightManager->vecLights[0].position.z += CAMERASPEED;
			break;
		case GLFW_KEY_S:		// Backwards (along z)
			::g_pLightManager->vecLights[0].position.z -= CAMERASPEED;
			break;
		case GLFW_KEY_Q:		// "Down" (along y axis)
			::g_pLightManager->vecLights[0].position.y -= CAMERASPEED;
			break;
		case GLFW_KEY_E:		// "Up" (along y axis)
			::g_pLightManager->vecLights[0].position.y += CAMERASPEED;
			break;
		case GLFW_KEY_1:
			::g_pLightManager->vecLights[0].attenuation.y *= 0.99f;	// less 1%
			break;
		case GLFW_KEY_2:
			::g_pLightManager->vecLights[0].attenuation.y *= 1.01f; // more 1%
			if (::g_pLightManager->vecLights[0].attenuation.y <= 0.0f)
			{
				::g_pLightManager->vecLights[0].attenuation.y = 0.001f;	// Some really tiny value
			}
			break;
		case GLFW_KEY_3:	// Quad
			::g_pLightManager->vecLights[0].attenuation.z *= 0.99f;	// less 1%
			break;
		case GLFW_KEY_4:	//  Quad
			::g_pLightManager->vecLights[0].attenuation.z *= 1.01f; // more 1%
			if (::g_pLightManager->vecLights[0].attenuation.z <= 0.0f)
			{
				::g_pLightManager->vecLights[0].attenuation.z = 0.001f;	// Some really tiny value
			}
			break;

		case GLFW_KEY_9:
			::g_bDrawDebugLightSpheres = true;
			break;
		case GLFW_KEY_0:
			::g_bDrawDebugLightSpheres = false;
			break;

		}// switch ( key )

	}//if ( bIsShiftPressedAlone(mods) )

	// HACK: print output to the console
	/*std::cout << "Light[0] linear atten: "
		<< ::g_pLightManager->vecLights[0].attenuation.y << ", "
		<< ::g_pLightManager->vecLights[0].attenuation.z << std::endl;*/
	return;
}


int main(void)
{

	GLFWwindow* window;
	//    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	//// Other uniforms:
	// Moved to "global" scope 
	// (you might want to place these inside the shader class)
	//GLint uniLoc_materialDiffuse = -1;	
	//GLint uniLoc_materialAmbient = -1;   
	//GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
	//GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
	//							// w = shininess of the 
	//GLint uniLoc_eyePosition = -1;	// Camera position
	//GLint uniLoc_mModel = -1;
	//GLint uniLoc_mView = -1;
	//GLint uniLoc_mProjection = -1;


	if (!glfwInit())
		exit(EXIT_FAILURE);


	// Print to the console...(if a console is there)
	//std::cout << "Hello" << std::endl;
	//int q = 8;
	//std::cout << "Type a number:";
	//std::cin >> q;
	//std::cout << "You typed " << q << ". Hazzah." << std::endl;

	int height = 480;	/* default */
	int width = 640;	// default
	std::string title = "OpenGL Rocks";

	std::ifstream infoFile("config.txt");
	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}
	else
	{	// File DID open, so read it... 
		std::string a;

		infoFile >> a;	// "Game"	//std::cin >> a;
		infoFile >> a;	// "Config"
		infoFile >> a;	// "width"

		infoFile >> width;	// 1080

		infoFile >> a;	// "height"

		infoFile >> height;	// 768

		infoFile >> a;		// Title_Start

		std::stringstream ssTitle;		// Inside "sstream"
		bool bKeepReading = true;
		do
		{
			infoFile >> a;		// Title_End??
			if (a != "Title_End")
			{
				ssTitle << a << " ";
			}
			else
			{	// it IS the end! 
				bKeepReading = false;
				title = ssTitle.str();
			}
		} while (bKeepReading);


	}//if ( ! infoFile.is_open() )




	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// C++ string
	// C no strings. Sorry. char    char name[7] = "Michael\0";
	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;


	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;


	// Load models
	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	std::string error;
	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}
	//LoadModelsIntoScene();


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	// Get the uniform locations for this shader
	mvp_location = glGetUniformLocation(currentProgID, "MVP");		// program, "MVP");
	uniLoc_materialDiffuse = glGetUniformLocation(currentProgID, "materialDiffuse");
	uniLoc_materialAmbient = glGetUniformLocation(currentProgID, "materialAmbient");
	uniLoc_ambientToDiffuseRatio = glGetUniformLocation(currentProgID, "ambientToDiffuseRatio");
	uniLoc_materialSpecular = glGetUniformLocation(currentProgID, "materialSpecular");

	uniLoc_bIsDebugWireFrameObject = glGetUniformLocation(currentProgID, "bIsDebugWireFrameObject");

	uniLoc_eyePosition = glGetUniformLocation(currentProgID, "eyePosition");

	uniLoc_mModel = glGetUniformLocation(currentProgID, "mModel");
	uniLoc_mView = glGetUniformLocation(currentProgID, "mView");
	uniLoc_mProjection = glGetUniformLocation(currentProgID, "mProjection");

	//	GLint uniLoc_diffuseColour = glGetUniformLocation( currentProgID, "diffuseColour" );

	::g_pCollisionManager = new cCollisionManager();

	::g_pLightManager = new cLightManager();

	//::g_pLightManager->CreateLights(10);	// There are 10 lights in the shader


	LoadModelsLightsFromFile();
	::g_pLightManager->LoadShaderUniformLocations(currentProgID);

	::g_pLightManager->vecLights[0].attenuation.y = 0.1f;
	::g_pLightManager->vecLights[0].attenuation.z = 0.0f;



	glEnable(GL_DEPTH);

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();

	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		//        glm::mat4x4 m, p, mvp;			//  mat4x4 m, p, mvp;
		glm::mat4x4 p, mvp;			//  mat4x4 m, p, mvp;

		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);

		// Clear colour AND depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//glEnable(GL_DEPTH_TEST);

		//        glUseProgram(program);
		::g_pShaderManager->useShaderProgram("mySexyShader");
		GLint shaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

		// Update all the light uniforms...
		// (for the whole scene)
		::g_pLightManager->CopyLightInformationToCurrentShader();

		// Projection and view don't change per scene (maybe)
		p = glm::perspective(0.6f,			// FOV
			ratio,		// Aspect ratio
			0.1f,			// Near (as big as possible)
			1000.0f);	// Far (as small as possible)

// View or "camera" matrix
		glm::mat4 v = glm::mat4(1.0f);	// identity

		if (::g_cameraOnBall)
		{
			//glm::vec3 cameraXYZ = glm::vec3( 0.0f, 0.0f, 5.0f );	// 5 units "down" z
			v = glm::lookAt(glm::vec3(::g_vecGameObjects[3]->position.x, ::g_vecGameObjects[3]->position.y + 10, ::g_vecGameObjects[3]->position.z + 30),						// "eye" or "camera" position
				::g_vecGameObjects[3]->position,		// "At" or "target" 
				glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
		}
		else
		{
			//glm::vec3 cameraXYZ = glm::vec3( 0.0f, 0.0f, 5.0f );	// 5 units "down" z
			v = glm::lookAt(g_cameraXYZOrig,						// "eye" or "camera" position
				g_cameraTarget_XYZ,		// "At" or "target" 
				glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
		}

		glUniformMatrix4fv(uniLoc_mView, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(v));
		glUniformMatrix4fv(uniLoc_mProjection, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(p));


		// Draw the scene
		unsigned int sizeOfVector = ::g_vecGameObjects.size();	//*****//
		for (int index = 0; index != sizeOfVector; index++)
		{
			cGameObject* pTheGO = ::g_vecGameObjects[index];

			DrawObject(pTheGO);

			//			}// if ( ::g_pVAOManager->lookupVAOFromName(

		}//for ( int index = 0...


		// 
		if (::g_bDrawDebugLightSpheres)
		{
			//DEBUG sphere
			DrawDebugSphere(glm::vec3(0.0f, 0.0f, 0.0f),
				glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), 1.0f);
			// Light at 95% 
			float scaleAt99 = ::g_pLightManager->vecLights[0].calcApproxDistFromAtten(0.99f);
			DrawDebugSphere(::g_pLightManager->vecLights[0].position,
				glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), scaleAt99);
			// Light at 50% 
			float scaleAt50 = ::g_pLightManager->vecLights[0].calcApproxDistFromAtten(0.5f);
			DrawDebugSphere(::g_pLightManager->vecLights[0].position,
				glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), scaleAt50);
			// Light at 25% 
			float scaleAt25 = ::g_pLightManager->vecLights[0].calcApproxDistFromAtten(0.25f);
			DrawDebugSphere(::g_pLightManager->vecLights[0].position,
				glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), scaleAt25);
			// Light at 1% 
			float scaleAt01 = ::g_pLightManager->vecLights[0].calcApproxDistFromAtten(0.01f);
			DrawDebugSphere(::g_pLightManager->vecLights[0].position,
				glm::vec4(0.0f, 1.0f, 1.0f, 1.0f), scaleAt01);
		}//if ( ::g_bDrawDebugLightSpheres )


		//

		/*std::stringstream ssTitle;
		ssTitle << "Camera (xyz): "
			<< g_cameraXYZOrig.x << ", "
			<< g_cameraXYZOrig.y << ", "
			<< g_cameraXYZOrig.z;
		glfwSetWindowTitle( window, ssTitle.str().c_str() );*/


		// **************************************************
		// Physics being updated BEFORE the present 
			// Essentially the "frame time"
		// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		PhysicsStep(deltaTime);

		lastTimeStep = curTime;
		// **************************************************


		// "Presents" what we've drawn
		// Done once per scene 
		glfwSwapBuffers(window);
		glfwPollEvents();



	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(window);
	glfwTerminate();

	// 
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;
	delete ::g_pLightManager;
	delete ::g_pCollisionManager;

	//    exit(EXIT_SUCCESS);
	return 0;
}

void DrawClosestPointsOnTerrainTriangles(glm::vec3 thePoint, std::string name)
{
	std::string meshToDraw = name;	//::g_GameObjects[index]->meshName;

	sVAOInfo VAODrawInfo;
	if (::g_pVAOManager->lookupVAOFromName(meshToDraw, VAODrawInfo) == false)
	{	// Didn't find mesh
		return;
	}
	int numberOfTriangles = VAODrawInfo.vecPhysTris.size();

	for (int triIndex = 0; triIndex != numberOfTriangles; triIndex++)
	{
		// Get reference object for current triangle
		// (so the line below isn't huge long...)
		cPhysTriangle& curTriangle = VAODrawInfo.vecPhysTris[triIndex];

		glm::vec3 theClosestPoint = curTriangle.ClosestPtPointTriangle(thePoint);

		DrawDebugSphere(theClosestPoint, glm::vec4(1, 1, 1, 1), 0.1f);

		//collisionThingy:
		//	- Which objects collided (sphere - triangle/sphere - sphere)
		//	- Where, the speed, etc.


		//	vecMyColisions.push_Back( collisionThingy );
			// Calc response...
	//		glm::reflect( 
	}

	return;
}

bool GetCollidingTriangles(cGameObject* pSphere, cGameObject* pTriangleObject, std::vector<cPhysTriangle>& closestTriangles)
{
	// std::vector< cPhysTriangle > vecPhysTris;
	sVAOInfo VAODrawInfo;
	::g_pVAOManager->lookupVAOFromName(pTriangleObject->meshName, VAODrawInfo);
	int numberOfTriangles = VAODrawInfo.vecPhysTris.size();

	int activeIndex = -1;
	for (int triIndex = 0; triIndex != numberOfTriangles; triIndex++)
	{
		cPhysTriangle curTriangle = VAODrawInfo.vecPhysTris[triIndex];
		//////////////////////////////////
		//Below are the steps needed if the game object can be moved, not just a static model
		//at position 0
		if (pTriangleObject->editable) {
			glm::vec4 tempVec1(curTriangle.vertex[0].x, curTriangle.vertex[0].y, curTriangle.vertex[0].z, 1);
			tempVec1 = pTriangleObject->mModel * tempVec1;

			glm::vec4 tempVec2(curTriangle.vertex[1].x, curTriangle.vertex[1].y, curTriangle.vertex[1].z, 1);
			tempVec2 = pTriangleObject->mModel * tempVec2;

			glm::vec4 tempVec3(curTriangle.vertex[2].x, curTriangle.vertex[2].y, curTriangle.vertex[2].z, 1);
			tempVec3 = pTriangleObject->mModel * tempVec3;

			curTriangle.vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
			curTriangle.vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
			curTriangle.vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
		}
		////////////////////////////////////////////////////////////////////////
		glm::vec3 theClosestPoint = curTriangle.ClosestPtPointTriangle(pSphere->position);
		float thisDistance = glm::distance(pSphere->position, theClosestPoint);
		if (thisDistance < pSphere->radius)
		{
			closestTriangles.push_back(curTriangle);
		}

	}

	if (closestTriangles.size() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
	//return VAODrawInfo.vecPhysTris[activeIndex];
}

//void calcTriangleSphereCollision(cPhysTriangle theTriangle, glm::vec3& velocity, cGameObject* pCurGO)
//{
//	//Below are the steps needed if the game object can be moved, not just a static model
//	//at position 0
//
//	if (pCurGO->editable) {
//		glm::vec4 tempVec1(theTriangle.vertex[0].x, theTriangle.vertex[0].y, theTriangle.vertex[0].z, 1);
//		tempVec1 = pCurGO->mModel * tempVec1;
//
//		glm::vec4 tempVec2(theTriangle.vertex[1].x, theTriangle.vertex[1].y, theTriangle.vertex[1].z, 1);
//		tempVec2 = pCurGO->mModel * tempVec2;
//
//		glm::vec4 tempVec3(theTriangle.vertex[2].x, theTriangle.vertex[2].y, theTriangle.vertex[2].z, 1);
//		tempVec3 = pCurGO->mModel * tempVec3;
//
//		theTriangle.vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
//		theTriangle.vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
//		theTriangle.vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
//	}
//
//	//taken from one source, pretty darn sure calculating the normal works
//	glm::vec3 U = theTriangle.vertex[1] - theTriangle.vertex[0];
//	glm::vec3 V = theTriangle.vertex[2] - theTriangle.vertex[0];
//	glm::vec3 normal = glm::cross(U, V);
//	normal = glm::normalize(normal);
//
//	//got from another source
//	glm::vec3 reflected = (glm::dot(velocity, normal) / glm::dot(normal, normal))* normal;
//	glm::vec3 w = velocity - reflected;
//	velocity = w - reflected;
//
//}

// Update the world 1 "step" in time
void PhysicsStep(double deltaTime)
{
	// Use the light loctation (because we can move it)


	const glm::vec3 GRAVITY = glm::vec3(0.0f, -9.0f, 0.0f);

	::g_pCollisionManager->ClearCollisions();

	// Identical to the 'render' (drawing) loop
	for (int index = 0; index != ::g_vecGameObjects.size(); index++)
	{
		cGameObject* pCurGO = ::g_vecGameObjects[index];

		// Is this object to be updated?
		if (pCurGO->bIsUpdatedInPhysics)
		{	// DON'T update this
			// Explicit Euler  (RK4) 
			// New position is based on velocity over time
			glm::vec3 deltaPosition = (float)deltaTime * pCurGO->vel;
			pCurGO->position += deltaPosition;
			pCurGO->oldPosition = pCurGO->position;

			// New velocity is based on acceleration over time
			if (pCurGO->affectedByGravity) {
				glm::vec3 deltaVelocity = ((float)deltaTime * pCurGO->accel)
					+ ((float)deltaTime * GRAVITY);

				pCurGO->vel += deltaVelocity;
				pCurGO->holderVel = pCurGO->vel;
			}
		}



		// HACK: Collision step should be its own function
		switch (pCurGO->typeOfObject)
		{
		case eTypeOfObject::SPHERE:
		{
			//if the ball is out of bounds, put it back in
			if (pCurGO->position.y < -30)
			{
				pCurGO->position.x = getRandInRange(-20.0f, 10.0f);
				pCurGO->position.y = getRandInRange(4.0f, 10.0f);
				pCurGO->position.z = getRandInRange(0.0f, -40.0f);
				pCurGO->vel.z = getRandInRange(-1.0f, 1.0f);
				pCurGO->vel.x = getRandInRange(-1.0f, 1.0f);
				pCurGO->vel.y = 0.0f;
				continue;
			}
			// Compare this to EVERY OTHER object in the scene
			pCurGO->collidedThisFrame = false;
			cCollision collision;
			collision.mainObject = pCurGO;
			pCurGO->orientation2.x -= 0.03;

			for (int indexEO = 0; indexEO != ::g_vecGameObjects.size(); indexEO++)
			{
				// Don't test for myself
				if (index == indexEO)
					continue;	// It's me!! 

				cGameObject* pOtherObject = ::g_vecGameObjects[indexEO];
				// Is Another object
				switch (pOtherObject->typeOfObject)
				{
				case eTypeOfObject::SPHERE:
					// 
					if (PenetrationTestSphereSphere(pCurGO, pOtherObject))
					{
						////std::cout << "Collision!" << std::endl;
						//HACK: so we dont get double collision response right now

						collision.otherGameObjects.push_back(pOtherObject);
						if (!pCurGO->collidedThisFrame) {
							pCurGO->collidedThisFrame = true;
							pCurGO->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
							//pOtherObject->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
							//pOtherObject->collidedThisFrame = true;
							//BounceCalcSphereSphere(pCurGO, pOtherObject);
						}
					}
					break;
				case eTypeOfObject::PLANE: case eTypeOfObject::CUBE:
					//find the closest triangle on the place and if it is touching
					//do a collision response
					//glm::vec3 thePointToTest = pCurGO->position;

					//DEBUG for seeing where the points are
					//DrawClosestPointsOnTerrainTriangles(thePointToTest, pOtherObject->meshName);

					float distance = 99999;
					//will have to find if multiple triangles were in range
					std::vector<cPhysTriangle> closestTriangles;
					//cPhysTriangle closestTriangle = GetCollidingTriangles(pCurGO, pOtherObject, closestTriangles);
					if (GetCollidingTriangles(pCurGO, pOtherObject, closestTriangles))
					{
						pCurGO->collidedThisFrame = true;
						int trianglesSize = closestTriangles.size();
						if (trianglesSize > 1)
						{
							std::cout << "Multi-Triangle Collision" << std::endl;
						}
						for (int i = 0; i < trianglesSize; i++)
						{
							//cPhysTriangle* triPointer = new cPhysTriangle();
							//triPointer = closestTriangles[i];
							std::pair<cPhysTriangle, cGameObject*> thePair;
							thePair = std::make_pair(closestTriangles[i], pOtherObject);
							collision.triangles.push_back(thePair);
						}
					}
					break;

				}//switch ( pOtherObject->typeOfObject )

			}

			//if it didnt collide, set its colour back to original
			if (!pCurGO->collidedThisFrame)
			{
				pCurGO->diffuseColour = pCurGO->originalColour;
			}
			else
			{
				::g_pCollisionManager->collisions.push_back(collision);
			}
			break;
		}
		case eTypeOfObject::CUBE:
			if (pCurGO->position.x > 0)
			{
				pCurGO->vel.x = -0.3;
			}
			else if (pCurGO->position.x < -30)
			{
				pCurGO->vel.x = 0.3;
			}
			break;
		};



	}//for ( int index...

	::g_pCollisionManager->CalculateCollisions();

	return;
}


// Used by the light drawing thingy
// Will draw a wireframe sphere at this location with this colour
void DrawDebugSphere(glm::vec3 location, glm::vec4 colour,
	float scale)
{
	glm::vec3 oldPosition = ::g_pTheDebugSphere->position;
	glm::vec4 oldDiffuse = ::g_pTheDebugSphere->diffuseColour;
	bool bOldIsWireFrame = ::g_pTheDebugSphere->bIsWireFrame;

	::g_pTheDebugSphere->position = location;
	::g_pTheDebugSphere->diffuseColour = colour;
	::g_pTheDebugSphere->bIsWireFrame = true;
	::g_pTheDebugSphere->scale = scale;

	DrawObject(::g_pTheDebugSphere);

	::g_pTheDebugSphere->position = oldPosition;
	::g_pTheDebugSphere->diffuseColour = oldDiffuse;
	::g_pTheDebugSphere->bIsWireFrame = bOldIsWireFrame;

	return;
}

// Draw a single object
void DrawObject(cGameObject* pTheGO)
{
	// Is there a game object? 
	if (pTheGO == 0)	//if ( ::g_GameObjects[index] == 0 )
	{	// Nothing to draw
		return;		// Skip all for loop code and go to next
	}

	// Was near the draw call, but we need the mesh name
	std::string meshToDraw = pTheGO->meshName;		//::g_GameObjects[index]->meshName;

	sVAOInfo VAODrawInfo;
	if (::g_pVAOManager->lookupVAOFromName(meshToDraw, VAODrawInfo) == false)
	{	// Didn't find mesh
		return;
	}

	// There IS something to draw
	glm::mat4x4 mModel = glm::mat4x4(1.0f);	//		mat4x4_identity(m);
	// (You'll see this as a "model" or a "world")

	glm::mat4 matRreRotZ = glm::mat4x4(1.0f);
	matRreRotZ = glm::rotate(matRreRotZ, pTheGO->orientation.z,
		glm::vec3(0.0f, 0.0f, 1.0f));
	mModel = mModel * matRreRotZ;

	glm::mat4 trans = glm::mat4x4(1.0f);
	trans = glm::translate(trans,
		pTheGO->position);
	mModel = mModel * trans;

	glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
	matPostRotZ = glm::rotate(matPostRotZ, pTheGO->orientation2.z,
		glm::vec3(0.0f, 0.0f, 1.0f));
	mModel = mModel * matPostRotZ;

	//			::g_vecGameObjects[index]->orientation2.y += 0.01f;

	glm::mat4 matPostRotY = glm::mat4x4(1.0f);
	matPostRotY = glm::rotate(matPostRotY, pTheGO->orientation2.y,
		glm::vec3(0.0f, 1.0f, 0.0f));
	mModel = mModel * matPostRotY;


	glm::mat4 matPostRotX = glm::mat4x4(1.0f);
	matPostRotX = glm::rotate(matPostRotX, pTheGO->orientation2.x,
		glm::vec3(1.0f, 0.0f, 0.0f));
	mModel = mModel * matPostRotX;

	// assume that scale to unit bounding box
	// ************* BEWARE *****************
//			float finalScale = VAODrawInfo.scaleForUnitBBox * ::g_vecGameObjects[index]->scale;
	// We have taken out the scale adjustment so the scale is AS IT IS FROM THE MODEL
	float finalScale = pTheGO->scale;

	glm::mat4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale,
		glm::vec3(finalScale,
			finalScale,
			finalScale));
	mModel = mModel * matScale;

	pTheGO->mModel = mModel;


	glUniformMatrix4fv(uniLoc_mModel, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(mModel));

	glm::mat4 mWorldInTranpose = glm::inverse(glm::transpose(mModel));
	//pTheGO->mModel = mWorldInTranpose;

	glUniform4f(uniLoc_materialDiffuse,
		pTheGO->diffuseColour.r,
		pTheGO->diffuseColour.g,
		pTheGO->diffuseColour.b,
		pTheGO->diffuseColour.a);
	//...and all the other object material colours

	if (pTheGO->bIsWireFrame)
	{
		glUniform1f(uniLoc_bIsDebugWireFrameObject, 1.0f);	// TRUE
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	// Default											//		glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glUniform1f(uniLoc_bIsDebugWireFrameObject, 0.0f);	// FALSE
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// Default
		glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
		glEnable(GL_CULL_FACE);
	}

	glCullFace(GL_BACK);


	glBindVertexArray(VAODrawInfo.VAO_ID);

	glDrawElements(GL_TRIANGLES,
		VAODrawInfo.numberOfIndices,		// testMesh.numberOfTriangles * 3,	// How many vertex indices
		GL_UNSIGNED_INT,					// 32 bit int 
		0);
	// Unbind that VAO
	glBindVertexArray(0);

	return;
}


bool bIsShiftPressed(int mods)
{
	if (mods & GLFW_MOD_SHIFT)
	{
		return true;
	}
	return false;
}

bool bIsCtrlPressed(int mods)
{
	if (mods & GLFW_MOD_CONTROL)
	{
		return true;
	}
	return false;
}

bool bIsAltPressed(int mods)
{
	if (mods & GLFW_MOD_ALT)
	{
		return true;
	}
	return false;
}


bool bIsShiftPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_SHIFT) == GLFW_MOD_SHIFT)
	{
		return true;
	}
	return false;
}

bool bIsCtrlPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL)
	{
		return true;
	}
	return false;
}

bool bIsAltPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_ALT) == GLFW_MOD_ALT)
	{
		return true;
	}
	return false;
}