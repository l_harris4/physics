#ifndef _cCOLLISION_HG_
#define _cCOLLISION_HG_
#include <vector>
#include <glm\vec3.hpp>
#include <utility>
//forward declarations of needed classes
class cPhysTriangle;
class cGameObject;


class cCollision
{
public:
	cCollision();
	~cCollision();
	cGameObject* mainObject;
	std::vector<std::pair<cPhysTriangle,cGameObject*>> triangles;
	std::vector<cGameObject*> otherGameObjects;
	void calcCollision();

	//void calcTriangleSphereCollision(cPhysTriangle theTriangle, glm::vec3& velocity, cGameObject* pCurGO);
};


#endif // !_cCOLLISION_HG_

