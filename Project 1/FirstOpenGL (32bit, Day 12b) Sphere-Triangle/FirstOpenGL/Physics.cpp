#include "Physics.h"
#include "cGameObject.h"
#include "cTriangle.h"

#include <glm/glm.hpp>		// glm::distance
#include <glm/geometric.hpp>

bool PenetrationTestSphereSphere(cGameObject* pA, cGameObject* pB)
{
	// If the distance between the two sphere is LT the sum or the radii,
	//	they are touching or penetrating

	float totalRadii = pA->radius + pB->radius;

	// The Pythagorean distance 
	float distance = glm::distance(pA->position, pB->position);

	if (distance <= totalRadii)
	{
		return true;
	}

	return false;
}

void calcTriangleSphereCollision(cGameObject* sphereObject, cPhysTriangle* theTriangle, cGameObject* triangleObject)
{
	//Below are the steps needed if the game object can be moved, not just a static model
	//at position 0

	if (triangleObject->editable) {
		glm::vec4 tempVec1(theTriangle->vertex[0].x, theTriangle->vertex[0].y, theTriangle->vertex[0].z, 1);
		tempVec1 = triangleObject->mModel * tempVec1;

		glm::vec4 tempVec2(theTriangle->vertex[1].x, theTriangle->vertex[1].y, theTriangle->vertex[1].z, 1);
		tempVec2 = triangleObject->mModel * tempVec2;

		glm::vec4 tempVec3(theTriangle->vertex[2].x, theTriangle->vertex[2].y, theTriangle->vertex[2].z, 1);
		tempVec3 = triangleObject->mModel * tempVec3;

		theTriangle->vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
		theTriangle->vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
		theTriangle->vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
	}

	//taken from one source, pretty darn sure calculating the normal works
	glm::vec3 U = theTriangle->vertex[1] - theTriangle->vertex[0];
	glm::vec3 V = theTriangle->vertex[2] - theTriangle->vertex[0];
	glm::vec3 normal = glm::cross(U, V);
	normal = glm::normalize(normal);

	//got from another source
	glm::vec3 reflected = (glm::dot(sphereObject->holderVel, normal) / glm::dot(normal, normal))* normal;
	glm::vec3 w = sphereObject->holderVel - reflected;

	sphereObject->vel = w - reflected;

}

void calcTrianglesSphereCollision(cGameObject * sphereObject, std::vector<std::pair<cPhysTriangle, cGameObject*>> &theTriangles)
{
	/*TODO, pretty much same function as above, but you have to go through all the triangles,
	combining their normals, before using that normal on the velocity of the sphere*/
	int trianglesSize = theTriangles.size();
	glm::vec3 finalNormal = glm::vec3(0.0f, 0.0f, 0.0f);
	for (int triIndex = 0; triIndex < trianglesSize; triIndex++)
	{
		cPhysTriangle* curTriangle = &theTriangles[triIndex].first;
		cGameObject * curObject = theTriangles[triIndex].second;
		if (curObject->editable) {
			
			glm::vec4 tempVec1(curTriangle->vertex[0].x, curTriangle->vertex[0].y, curTriangle->vertex[0].z, 1);
			tempVec1 = curObject->mModel * tempVec1;

			glm::vec4 tempVec2(curTriangle->vertex[1].x, curTriangle->vertex[1].y, curTriangle->vertex[1].z, 1);
			tempVec2 = curObject->mModel * tempVec2;

			glm::vec4 tempVec3(curTriangle->vertex[2].x, curTriangle->vertex[2].y, curTriangle->vertex[2].z, 1);
			tempVec3 = curObject->mModel * tempVec3;

			curTriangle->vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
			curTriangle->vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
			curTriangle->vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
		}

		//taken from one source, pretty darn sure calculating the normal works
		glm::vec3 U = curTriangle->vertex[1] - curTriangle->vertex[0];
		glm::vec3 V = curTriangle->vertex[2] - curTriangle->vertex[0];
		glm::vec3 normal = glm::cross(U, V);
		normal = glm::normalize(normal);
		finalNormal += normal;
	}

	finalNormal = glm::normalize(finalNormal);
	//got from another source
	glm::vec3 reflected = (glm::dot(sphereObject->holderVel, finalNormal) / glm::dot(finalNormal, finalNormal))* finalNormal;
	glm::vec3 w = sphereObject->holderVel - reflected;

	sphereObject->vel = w - reflected;
}


//borrowed from internet
void BounceCalcSphereSphere(cGameObject* pA, cGameObject* pB)
{
	// First, find the normalized vector n from the center of 
	// circle1 to the center of circle2
	glm::vec3 n = pA->position - pB->position;
	glm::normalize(n);
	// Find the length of the component of each of the movement
	// vectors along n. 
	// a1 = v1 . n
	// a2 = v2 . n
	float a1 = glm::dot(pA->holderVel, n);
	float a2 = glm::dot(pB->holderVel, n);

	// Using the optimized version, 
	// optimizedP =  2(a1 - a2)
	//              -----------
	//                m1 + m2
	float optimizedP = (2.0 * (a1 - a2)) / (2/*HACK:assumed total mass of 2 right now*/);

	// Calculate v1', the new movement vector of circle1
	// v1' = v1 - optimizedP * m2 * n
	glm::vec3 v1 = pA->holderVel - optimizedP * 1 * n;   //1 is a placeholder for mass

	// Calculate v1', the new movement vector of circle1
	// v2' = v2 + optimizedP * m1 * n
	glm::vec3 v2 = pB->holderVel + optimizedP * 1 * n; //1 is a placeholder for mass

	pA->vel = glm::vec3(v1.x*0.5,v1.y*0.5,v1.z*0.5);//used to be pA->vel = v1;  but wanted to scale down
	//pB->vel = glm::vec3(v2.x*0.5, v2.y*0.5, v2.z*0.5);  //used to calculate both here, but wont anymore becase
			//this function will be called on all spheres in a collision
}

void BounceCalcSphereSpheres(cGameObject * mainObject, std::vector<cGameObject*> otherObjects)
{
	glm::vec3 resultingVel = glm::vec3(0.0f, 0.0f, 0.0f);
	int objectsSize = otherObjects.size();

	//calculate a distance to move the objects away from eachother
	glm::vec3 distanceVector = glm::vec3(0.0f, 0.0f, 0.0f);
	for (int objectIndex = 0; objectIndex < otherObjects.size(); objectIndex++)
	{
		glm::vec3 n = mainObject->position - otherObjects[objectIndex]->oldPosition;
		glm::normalize(n);

		float a1 = glm::dot(mainObject->holderVel, n);
		float a2 = glm::dot(otherObjects[objectIndex]->holderVel, n);

		float optimizedP = (2.0 * (a1 - a2)) / (2/*HACK:assumed total mass of 2 right now*/);

		glm::vec3 v1 = mainObject->holderVel - optimizedP * 1 * n;   //1 is a placeholder for mass

		//calculate distance for the distance vector
		float distance = glm::distance(mainObject->position, otherObjects[objectIndex]->oldPosition);
		distance = (distance / (mainObject->radius + otherObjects[objectIndex]->radius));
		distanceVector += distance * glm::normalize(v1);

		//glm::vec3 v2 = otherObjects[objectIndex]->holderVel + optimizedP * 1 * n; //1 is a placeholder for mass
		resultingVel += v1*(distance*0.6f);
	}
	
	//moving the object out of the other sphere
	mainObject->position += distanceVector;

	//mainObject->position += glm::vec3(distanceVector.x*0.5, distanceVector.y*0.5, distanceVector.z*0.5);


	mainObject->vel = resultingVel;
	//would have to go through all the spheres, adding the resulting velocity, then maybe normalize it
	//then apply that to the main object
}


// inline?? 
glm::vec3 cPhysTriangle::ClosestPtPointTriangle(glm::vec3 p)
{
	return this->ClosestPtPointTriangle(p, this->vertex[0],
		this->vertex[1],
		this->vertex[2]);
}

glm::vec3 cPhysTriangle::ClosestPtPointTriangle(glm::vec3 p, glm::vec3 a,
	glm::vec3 b, glm::vec3 c)
{
	// Check if P in vertex region outside A
	glm::vec3 ab = b - a;
	glm::vec3 ac = c - a;
	glm::vec3 ap = p - a;
	float d1 = glm::dot(ab, ap);
	float d2 = glm::dot(ac, ap);
	if (d1 <= 0.0f && d2 <= 0.0f) return a; // barycentric coordinates (1,0,0)

	// Check if P in vertex region outside B
	glm::vec3 bp = p - b;
	float d3 = glm::dot(ab, bp);
	float d4 = glm::dot(ac, bp);
	if (d3 >= 0.0f && d4 <= d3) return b; // barycentric coordinates (0,1,0)

	// Check if P in edge region of AB, if so return projection of P onto AB
	float vc = d1*d4 - d3*d2;
	if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f) {
		float v = d1 / (d1 - d3);
		return a + v * ab; // barycentric coordinates (1-v,v,0)
	}

	// Check if P in vertex region outside C
	glm::vec3 cp = p - c;
	float d5 = glm::dot(ab, cp);
	float d6 = glm::dot(ac, cp);
	if (d6 >= 0.0f && d5 <= d6) return c; // barycentric coordinates (0,0,1)

	// Check if P in edge region of AC, if so return projection of P onto AC
	float vb = d5*d2 - d1*d6;
	if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f) {
		float w = d2 / (d2 - d6);
		return a + w * ac; // barycentric coordinates (1-w,0,w)
	}

	// Check if P in edge region of BC, if so return projection of P onto BC
	float va = d3*d6 - d5*d4;
	if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f) {
		float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
		return b + w * (c - b); // barycentric coordinates (0,1-w,w)
	}

	// P inside face region. Compute Q through its barycentric coordinates (u,v,w)
	float denom = 1.0f / (va + vb + vc);
	float v = vb * denom;
	float w = vc * denom;
	return a + ab * v + ac * w; // = u*a + v*b + w*c, u = va * denom = 1.0f - v - w
}