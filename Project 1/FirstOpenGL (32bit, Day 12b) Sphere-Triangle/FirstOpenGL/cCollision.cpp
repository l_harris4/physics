#include "cCollision.h"
#include "cTriangle.h"
#include "cGameObject.h"
#include "Physics.h"
#include "cTriangle.h"

cCollision::cCollision()
{
}

cCollision::~cCollision()
{
	/*for (int i = 0; i < triangles.size(); i++)
	{
		delete triangles[i].first;
	}*/
}

void cCollision::calcCollision()
{
	int gameObjectsSize = otherGameObjects.size();
	/*for (int gameObjectIndex = 0; gameObjectIndex < gameObjectsSize; gameObjectIndex++)
	{
		BounceCalcSphereSphere(mainObject, otherGameObjects[gameObjectIndex]);
	}*/
	if (gameObjectsSize > 0)
	{
		BounceCalcSphereSpheres(mainObject, otherGameObjects);
	}
	int trianglesSize = triangles.size();
	if (trianglesSize > 0)
	{
		calcTrianglesSphereCollision(mainObject, triangles);
	}

	/*for (int triangleIndex = 0; triangleIndex < trianglesSize; triangleIndex++)
	{
		calcTriangleSphereCollision(mainObject, &triangles[triangleIndex].first, triangles[triangleIndex].second);
	}*/
}

////maybe this function should be in Physics.h
//void cCollision::calcTriangleSphereCollision(cPhysTriangle theTriangle, glm::vec3& velocity, cGameObject* pCurGO)
//{
//	//Below are the steps needed if the game object can be moved, not just a static model
//	//at position 0
//
//	if (pCurGO->editable) {
//		glm::vec4 tempVec1(theTriangle.vertex[0].x, theTriangle.vertex[0].y, theTriangle.vertex[0].z, 1);
//		tempVec1 = pCurGO->mModel * tempVec1;
//
//		glm::vec4 tempVec2(theTriangle.vertex[1].x, theTriangle.vertex[1].y, theTriangle.vertex[1].z, 1);
//		tempVec2 = pCurGO->mModel * tempVec2;
//
//		glm::vec4 tempVec3(theTriangle.vertex[2].x, theTriangle.vertex[2].y, theTriangle.vertex[2].z, 1);
//		tempVec3 = pCurGO->mModel * tempVec3;
//
//		theTriangle.vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
//		theTriangle.vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
//		theTriangle.vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
//	}
//
//	//taken from one source, pretty darn sure calculating the normal works
//	glm::vec3 U = theTriangle.vertex[1] - theTriangle.vertex[0];
//	glm::vec3 V = theTriangle.vertex[2] - theTriangle.vertex[0];
//	glm::vec3 normal = glm::cross(U, V);
//	normal = glm::normalize(normal);
//
//	//got from another source
//	glm::vec3 reflected = (glm::dot(velocity, normal) / glm::dot(normal, normal))* normal;
//	glm::vec3 w = velocity - reflected;
//	velocity = w - reflected;
//
//}
