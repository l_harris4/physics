#ifndef _cCOLLISIONMANAGER_HG_
#define _cCOLLISIONMANAGER_HG_

#include <vector>
class cCollision;

class cCollisionManager
{
public:
	cCollisionManager();
	~cCollisionManager();
	void AddCollision(cCollision collisionData);
	void ClearCollisions();
	void CalculateCollisions();
	//int addingIndex;

	std::vector<cCollision> collisions;
};

#endif
