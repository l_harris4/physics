#ifndef _Physics_HG_
#define _Physics_HG_

//#include "cGameObject.h"
#include <glm\vec3.hpp>
#include "cTriangle.h"
#include <vector>
class cGameObject;
//class cPhysTriangle;

// Sphere-Sphere
// Sphere-Plane
// Sphere-Triangle
// Triangle-Triangle
// Mesh-Mesh
// Sphere-Mesh

// Our objects are vectors of pointers, so we might as well pass pointers
bool PenetrationTestSphereSphere( cGameObject* pA, cGameObject* pB );
void BounceCalcSphereSphere(cGameObject* pA, cGameObject* pB);
void BounceCalcSphereSpheres(cGameObject* pA, std::vector<cGameObject*> otherObjects);
void calcTriangleSphereCollision(cGameObject* sphereObject, cPhysTriangle* theTriangle, cGameObject* triangleObject);
void calcTrianglesSphereCollision(cGameObject* sphereObject, std::vector<std::pair<cPhysTriangle,cGameObject*>> &theTriangles);



#endif
